require 'rails_helper'

RSpec.describe Goal, type: :model do

  describe "associations" do
    it "has_one address as addressable" do
      goal_has_one_address_helper
    end
    it "belongs_to account" do
      goal_belongs_to_account_helper
    end
    it "has_many balance_activities as itemable" do
      goal_has_many_balance_activities_as_itemable_helper
    end
  end

  describe "validations" do
    describe "valid" do
      it "should be valid" do
        goal_should_be_valid_helper
      end
    end
    describe "presence" do
      it "required fields" do
        fields = %w(signature amount status goal_type account_id end_time_unix
                    end_time_human start_time_unix start_time_human)
        common_validates_presence_for_fields_helper(:goal, fields)
      end
    end
    describe "numericality" do
      it "amount greater than 0" do
        goal_amount_greater_than_0_helper
      end
    end
    describe "uniqueness" do
      it "signature" do
        goal_signature_unique_helper
      end
    end
    describe "custom" do
      it "start_time_greater_than_last_30_seconds" do
        goal_custom_validation_start_time_greater_than_30_sec_helper
      end
      it "end_time_greater_than_start_time" do
        goal_custom_validation_end_time_greater_than_start_time_helper
      end
    end
  end

  describe "enums" do
    it "allows correct options for" do
      enums_with_values = [
        { goal_type: [:location_arrival] },
        { status: [:pending, :success, :failed] }
      ]
      common_confirm_enums_with_values_helper(:goal, enums_with_values)
    end
  end

  describe ".get_list_for_eth_address" do
    it "calls find_by_address with correct address" do
      goal_get_list_for_eth_address_calls_find_by_address_helper
    end
    it "returns status 400 if no account found" do
      goal_get_list_for_eth_address_returns_400_if_no_account_helper
    end
    it "returns correct account and address info" do
      goal_get_list_for_eth_address_returns_correct_info_helper
    end
  end

  describe ".update_all_pending_expired_goals" do
    it "calls and returns update_pending_expired_goals with self" do
      goals_update_all_pending_expired_goals_calls_update_pending_with_self_helper
    end
  end

  describe ".get_newly_expired_and_pending" do
    it "calls self with includes account" do
      goals_get_newly_expired_and_pending_includes_account_helper
    end
    it "returns pending expired goals with accounts" do
      goals_get_newly_expired_and_pending_returns_correct_goals_helper
    end
  end

  describe "#update_status_and_charge_if_should" do
    it "returns nil if status is not pending" do
      goal_update_status_and_charge_returns_nil_if_not_pending_helper
    end
    it "updates status" do
      goal_update_status_and_charge_updates_status_helper
    end
    it "calls BA.create-charge_ba if status is failed" do
      goal_update_status_and_charge_calls_create_charge_helper
    end
    it "doesn't call BA.create-charge_ba if status is not failed" do
      goal_update_status_and_charge_doesnt_call_create_charge_helper
    end
  end

  describe ".update_pending_expired_goals" do
    it "calls get_newly_expired_and_pending for correct goals" do
       goal_update_pending_expired_goals_calls_get_newly_expired_helper
    end
    it "calls #update_status_and_charge with failed for each goal" do
       goal_update_pending_expired_goals_calls_update_status_and_charge_helper
    end
  end

  describe "#update_as_success_or_fail_if_should" do
    it "returns nil if status is not pending" do
      goal_update_as_success_or_fail_returns_nil_if_not_pending_helper
    end
    it "returns nil if location is not close enough" do
      goal_update_as_success_or_fail_returns_nil_if_not_close_enough_helper
    end
    it "calls update_status_and_charge_if_should with failed status when should" do
      goal_update_as_success_or_fail_calls_update_status_with_failed_helper
    end
    it "calls update_status_and_charge_if_should with success status when should" do
      goal_update_as_success_or_fail_calls_update_status_with_success_helper
    end
  end

  describe ".verify_goal_completion_and_return_goal" do
    it "returns false if can't verify signature" do
      goal_verify_goal_completion_returns_false_if_sig_not_verified_helper
    end
    it "calls Goal.find" do
      goal_verify_goal_completion_calls_goal_find_helper
    end
    it "returns false if no goal" do
      goal_verify_goal_completion_returns_false_if_no_goal_helper
    end
    it "returns false if goal status is not pending" do
      goal_verify_goal_completion_returns_false_if_goal_not_pending_helper
    end
    it "calls Account.format_address with correct param" do
      goal_verify_goal_completion_calls_account_format_address_helper
    end
    it "returns false if goal account address does not match passed in coinbase" do
      goal_verify_goal_completion_returns_false_if_coinbase_doesnt_match_helper
    end
    it "returns goal" do
      goal_verify_goal_completion_returns_goal_helper
    end
  end

  describe ".attempt_to_update_as_completed" do
    it "calls .verify_goal_completion_and_return_goal" do
      goal_attempt_to_update_as_completed_calls_verify_goal_helper
    end
    it "returns false if verify method doesnt return a goal" do
      goal_attempt_to_update_as_completed_returns_false_if_no_goal_helper
    end
    it "calls update_as_success_or_fail_if_should with correct params" do
      goal_attempt_to_update_as_completed_calls_update_as_success_or_fail_helper
    end
    it "returns false if status is still pending" do
      goal_attempt_to_update_as_completed_returns_false_if_still_pending_helper
    end
    it "returns true if status is no longer pending" do
      goal_attempt_to_update_as_completed_returns_true_if_not_pending_helper
    end
  end

  describe ".validate_create_goal_data" do
    it "returns false if goal already exists with same signature" do
      goal_validate_create_goal_data_returns_false_if_already_sig_helper
    end
    it "returns false if signature can't be verified" do
      goal_validate_create_goal_data_returns_false_sig_not_valid_helper
    end
    it "returns false if account is not in data" do
      goal_validate_create_goal_data_returns_false_if_no_account_helper
    end
    it "returns false if amount requested is greater than usable_balance" do
      goal_validate_create_goal_data_returns_false_if_amount_too_much_helper
    end
    it "returns true if everything else passes and amount is not too much" do
      goal_validate_create_goal_data_returns_true_if_amount_not_too_much_helper
    end
  end

  describe ".add_extras_to_data" do
    it "calls EthHelpers.to_wei with correct param" do
      goals_calls_ethhelpers_to_wei_helper
    end
    it "adds amount_in wei to data" do
      goal_add_extras_to_data_adds_amount_in_wei_helper
    end
    it "calls Account.find_by_address with correct param" do
      goal_add_extras_to_data_calls_find_by_address_helper
    end
    it "adds account to data" do
      goal_add_extras_to_data_adds_account_to_data_helper
    end
    it "returns data" do
      goal_add_extras_to_data_returns_correct_data_helper
    end
  end

  describe ".create_goal" do
    it "calls .add_extras_to_data with correct param" do
      goal_create_goal_calls_add_extras_helper
    end
    it "returns false if goal data is not valid" do
      goal_create_goal_returns_false_if_goal_data_not_valid_helper
    end
    it "creates goal and address with correct info and returns true" do
      goal_create_goal_creates_goal_address_with_correct_data_helper
    end
  end

end
