require 'rails_helper'
RSpec.describe Location, type: :model do

  describe ".close_enough" do
    it "calls Geocoder::Calculations.distance_between with correct args" do
      location_close_enough_calls_distance_between_helper
    end
    it "returns true if less or equal to 100 meters" do
      location_close_enough_returns_true_helper
    end
    it "returns false if greater than 100 meters" do
      location_close_enough_returns_false_helper
    end
  end
end

