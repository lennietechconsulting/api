require 'rails_helper'

RSpec.describe Contract, type: :model do

  describe ".get_values_from_args" do
    it "returns the correct address and amount when 4 args exists" do
      contract_get_values_from_args_returns_correct_values_when_4_args_helper
    end
    it "returns the correct address and amount when 2 args exists" do
      contract_get_values_from_args_returns_correct_values_when_2_args_helper
    end
  end
  describe ".get_data_types" do
    it "returns array with correct data types when data is longer than 128" do
      contract_get_data_types_returns_4_types_when_should_helper
    end
    it "returns array with correct data types when data is not longer than 128" do
      contract_get_data_types_returns_2_types_when_should_helper
    end
  end

  describe ".create_client" do
    it "calls and returns a new ethereum client" do
      contract_create_client_returns_new_client_helper
    end
  end

  describe ".get_max_confirmed_block_number" do
    it "returns the current block number minus the required number of blocks" do
      contract_get_max_conf_block_num_returns_correct_to_block_number_helper
    end
  end

  describe ".get_raw_logs" do
    it "calls client.eth_get_logs with correct params" do
      contract_get_raw_logs_calls_client_eth_get_logs_helper
    end
    it "calls get logs with to_block param when passed in" do
      contract_get_raw_logs_calls_with_to_block_option_helper
    end
    it "calls get logs with latest when param not passed in" do
      contract_get_raw_logs_calls_with_no_to_block_option_helper
    end
    it "calls options from_block when passed in" do
      contract_get_raw_logs_calls_with_from_block_option_helper
    end
    it "calls default from_block when param not passed in" do
      contract_get_raw_logs_calls_with_from_block_default_helper
    end
    it "calls options topics when passed in" do
      contract_get_raw_logs_calls_topics_option_helper
    end
    it "calls default topics when param not passed in" do
      contract_get_raw_logs_calls_topics_default_helper
    end
  end

  describe ".get_args_for_transaction" do
    it "calls get_data_types with correct param" do
      contract_get_args_for_transaction_calls_get_data_types_helper
    end
    it "uses second log if second log is present" do
      contract_get_args_for_transaction_uses_second_log_helper
    end
    it "uses first log if second log is not present" do
      contract_get_args_for_transaction_uses_first_log_helper
    end
    it "calls decoder.decode with correct params for each type" do
      contract_get_args_for_transaction_calls_decode_helper
    end
  end

  describe ".get_confirmation_status" do
    it "returns confirmed when within min conf block number range" do
      contract_get_conf_status_returns_confirmed_helper
    end
    it "returns pending when not within min conf block number range" do
      contract_get_conf_status_returns_pending_helper
    end
  end

  describe ".get_formatted_data_from_raw_logs" do
    it "calls .get_values_from_args with correct params" do
      contract_get_formatted_data_calls_get_values_from_args_helper
    end
    it "calls client.eth_get_transaction_receipt with correct params" do
      contract_get_formatted_data_calls_eth_get_transaction_receipt_helper
    end
    it "calls .get_max_confirmed_block_number with correct params" do
      contract_get_formatted_data_calls_get_max_confirmed_block_helper
    end
    it "calls .get_args_for_transaction with correct params" do
      contract_get_formatted_data_calls_get_args_for_transaction_helper
    end
    it "calls .get_confirmation_status with correct params" do
      contract_get_formatted_data_calls_get_confirmation_status_helper
    end
    it "returns correct data" do
      contract_get_formatted_data_returns_correct_data_helper
    end
  end

  describe ".get_logs" do
    it "calls .create_client" do
      contract_get_logs_calls_create_client_helper
    end
    it "calls .get_raw_logs with correct params" do
      contract_get_logs_calls_get_raw_logs_helper
    end
    it "calls and returns .get_formatted_data_from_raw_logs with correct params" do
      contract_get_logs_calls_get_formatted_data_helper
    end
  end
end
