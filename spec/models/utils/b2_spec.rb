require 'rails_helper'

RSpec.describe B2, type: :model do

  describe "#authorize" do

    it "sends out correct request" do
      b2_authorize_sends_out_correct_request_helper
    end

    context "authorization passes" do
      it "sets instance variables correctly when authenticates" do
        b2_authorize_sets_instance_variables_correctly_helper
      end
    end

    context "authorization fails" do
      it "does not set instance variables" do
        b2_auth_fail_doesnt_set_instance_variables_helper
      end
    end
  end

  describe "#get_upload_url" do

    context "when authentication failed" do
      it "does not call get_upload_url endpoint" do
        b2_get_upload_url_does_not_call_get_upload_url_when_unauthorized_helper
      end

      it "does not set instance variables" do
        b2_get_upload_url_does_not_set_instance_variables_helper
      end
    end

    context "when authentication worked" do
      it "calls get_upload_url endpoint with proper body and headers" do
        b2_calls_get_upload_url_with_correct_args_helper
      end

      it "sets the proper instance variables" do
        b2_get_upload_url_sets_correct_variables_helper
      end
    end
  end

  describe ".upload_file" do

    context "when get_upload_url fails" do
      it "should not make upload file request" do
        b2_upload_file_doesnt_call_upload_when_get_upload_url_fails_helper
      end

      it "should return error" do
        b2_upload_file_returns_error_helper
      end
    end

    context "when upload_file fails" do
      it "should make upload file request" do
        b2_upload_file_upload_fails_makes_upload_request_helper
      end

      it "should return error" do
        b2_upload_file_upload_fails_returns_error_helper
      end
    end

    context "when upload_file succeeds" do
      it "should make upload file request with proper body and headers" do
        b2_upload_file_makes_correct_request_helper
      end

      it "should return proper success response" do
        b2_upload_file_returns_correct_response_helper
      end
    end
  end

  describe ".download_file_by_name" do
    it "should call authorize" do
      b2_test_that_authorize_is_called_during_download
    end

    context "when authentication fails" do
      it "should return proper error" do
        b2_test_that_download_returns_error_if_auth_fails
      end
      it "should not send download request" do
        b2_test_download_request_not_sent_if_auth_fails
      end
    end

    context "when authorization passes" do
      it "should send request with proper header and url" do
        b2_test_download_request_is_sent_with_proper_data_and_headers_helper
      end

      context "when download file fails" do
        it "should return error" do
          b2_test_download_request_fail_returns_error
        end
      end

      context "when download file succeeds" do

        context "when file corrupt" do
          it "should send proper error message" do
            b2_test_download_success_with_bad_sha1_returns_error
          end
        end

        context "when file good" do
          it "should send response with file" do
            b2_test_download_success_returns_proper_response
          end
        end
      end
    end
  end
end
