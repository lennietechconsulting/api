require 'rails_helper'
RSpec.describe EthHelpers, type: :model do

  describe ".to_wei" do
    it "returns the correct amount" do
      eth_helpers_to_wei_returns_correct_amount_helper
    end
  end
  describe ".to_ether" do
    it "returns the correct amount" do
      eth_helpers_to_ether_returns_correct_amount_helper
    end
  end
end

