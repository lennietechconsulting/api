require 'rails_helper'
RSpec.describe Signature, type: :model do

  describe ".verify_sig" do
    it "calls HTTParty.post with correct params" do
      sig_verify_sig_calls_httparty_post_helper
    end
    it "returns false if HTTParty throws an error" do
      sig_verify_sig_returns_false_if_error_helper
    end
    it "returns false if response body is not true" do
      sig_verify_sig_returns_false_if_response_body_is_false_helper
    end
    it "returns true if response body is true" do
      sig_verify_sig_returns_true_helper
    end
  end
end
