require 'rails_helper'

RSpec.describe Address, type: :model do

  describe "associations" do
    it "belongs_to addressable polymorphic" do
      address_has_one_addressable_helper
    end
  end

  describe "validations" do
    describe "valid" do
      it "should be valid" do
        address_should_be_valid_helper
      end
    end
    describe "presence" do
      it "required fields" do
        fields = %w(latitude longitude formatted_address addressable_type
                    addressable_id)
        common_validates_presence_for_fields_helper(:address, fields)
      end
    end
  end
end
