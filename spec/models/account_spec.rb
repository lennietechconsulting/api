require 'rails_helper'

RSpec.describe Account, type: :model do

  describe "associations" do
    it "has_many balance_activities" do
      account_has_many_balance_activities_helper
    end
    it "has_many goals" do
      account_has_many_goals_helper
    end
  end

  describe "validations"do
    it "should be valid" do
      account_should_be_valid_helper
    end
    context "presence" do
      it "for required fields" do
        fields = %w(eth_address balance created_at_block_number)
        common_validates_presence_for_fields_helper(:account, fields)
      end
    end
    context "uniqueness" do
      it "eth_address" do
        account_eth_address_unique_helper
      end
    end
    context "numericality" do
      it "created_at_block_number greater than 0" do
        account_created_at_block_number_greater_than_0_helper
      end
    end
    context "length" do
      it "eth_address is 40" do
        account_eth_address_is_40_helper
      end
    end
  end

  describe ".update_data_and_return_account" do
    it "returns fail if no account" do
      account_update_data_and_return_returns_fail_helper
    end
    it "calls find_by_address" do
      account_update_data_and_return_calls_find_by_address_helper
    end
    it "calls Goal.update_pending_expired_goals" do
      account_update_data_and_return_calls_goal_update_pending_helper
    end
    it "returns correct account info" do
      account_update_data_and_return_returns_correct_account_info_helper
    end
  end

  describe ".find_by_address" do
    it "calls find_by with correct address" do
      account_find_by_address_calls_find_by_with_formatted_address_helper
    end
    it "returns correct account" do
      account_find_by_address_calls_returns_correct_account_helper
    end
  end

  describe "#sum_of_total_pending_charges" do
    it "returns sum of all pending goal amounts for account" do
      account_sum_of_toal_pending_charges_helper
    end
  end

  describe "#usable_balance" do
    it "calls #sum_of_total_pending_charges" do
      account_usable_balance_calls_sum_of_total_pending_charges_helper
    end
    it "returns correct usable balance" do
      account_usable_balance_returns_correct_account_helper
    end
  end

  describe "#valid_deposits" do
    it "returns correct deposits" do
      account_valid_deposits_returns_correct_deposits_helper
    end
  end

  describe ".create_account" do
    it "creates and returns an account with correct data" do
      account_create_account_returns_new_account_helper
    end
    it "throws an exception with improper data" do
      account_create_account_returns_exception_helper
    end
  end

  describe ".find_or_create_account" do
    it "calls format_address" do
      account_find_or_create_account_calls_format_address_helper
    end
    it "calls create_account and returns new account when no account already" do
      account_find_or_create_account_creates_new_account_helper
    end
    it "calls find_by and returns already created account" do
      account_find_or_create_account_returns_old_account_helper
    end
  end

  describe ".format_address" do
    it "returns formatted address" do
      account_format_address_returns_correct_format_helper
    end
  end

end

