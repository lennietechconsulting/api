FactoryBot.define do
  factory :account do
    sequence :eth_address do |n|
      "4a00#{n}04770c12e76cc1de10660ab460f8a85a5be".truncate(40)
    end
    balance 0.00
    created_at_block_number 8

    factory :account_with_balance_activities do

      transient do
        balance_activities_count 3
      end

      after(:create) do |account, evaluator|
        create_list(:balance_activity,
                    evaluator.balance_activities_count,
                    account: account)
      end

    end
  end
end
