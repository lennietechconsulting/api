FactoryBot.define do
  factory :goal do
    account
    sequence :signature do |n|
      "0x7e#{n}e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d"
    end
    amount 100
    status 0
    goal_type 0
    end_time_unix { (Time.now + 1.day).to_i }
    end_time_human "June 5 9 am"
    start_time_unix { Time.now.to_i }
    start_time_human "June 6 9 am"
  end
end
