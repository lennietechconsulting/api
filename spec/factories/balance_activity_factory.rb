FactoryBot.define do
  factory :balance_activity do
    account
    sequence :transaction_hash do |n|
      "0x7e#{n}e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d"
    end
    activity_type "deposit"
    amount 500000000000000000
    credit_or_debit "credit"
    new_balance 500000000000000000
    old_balance 0.0
    block_number 8
    gas_used 23145
    transaction_status 1
    confirmation_status "confirmed"
  end
end
