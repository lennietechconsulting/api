FactoryBot.define do
  factory :address do
    latitude 12.34
    longitude 56.78
    formatted_address "123 5th street NYC"
  end
end

