require 'rails_helper'

RSpec.describe BalanceActivity, type: :model do

  describe "fetch logs first time" do
    # FETCH 1
    # fetch logs some are sucess some revert
    # some are confirmed and some are pending
    # creates account and balance_activity for each one
    # only updates balances and account balance for confirmed and sucessful logs
    it "should create correct number of bas and accounts" do
      fetching_should_have_four_accounts_and_bas_helper
    end
    it "should have one account and one revert pending ba" do
      fetching_should_have_one_account_one_revert_pending_ba_helper
    end
    it "revert pending ba should have account and itself with 0 balances" do
      fetching_should_have_revert_pending_ba_with_0_balance_helper
    end
    it "should have one account and one success pending ba" do
      fetching_should_have_one_account_one_success_pending_ba_helper
    end
    it "success pending ba should have account and itself with 0 balances" do
      fetching_should_have_success_pending_ba_with_0_balance_helper
    end
    it "should have one account and one revert confimed ba" do
      fetching_should_have_one_account_one_revert_confirmed_ba_helper
    end
    it "revert confirmed ba should have account and itself with 0 balances" do
      fetching_should_have_revert_confirmed_ba_with_0_balance_helper
    end
    it "should have one account and one success confimed ba" do
      fetching_should_have_one_account_one_success_confirmed_ba_helper
    end
    it "success confirmed ba should update balances for account and itself" do
      fetching_should_have_success_confirmed_ba_with_updated_balances_helper
    end
  end

  describe "fetch logs second time" do
    #
    # FETCH 2
    # logs:
    # 4 from before and 2 new ones
    # One now success and confirmed. Updates balances
    # One now revert and confirmed. Doesn't update balances
    # other two do not change and do not update balances
    # New one has already made account and associates with that
    # previous account and updates that account
    # New one has already made account and associates with that
    # previous account but doesn't update it
    # doesn't recreate the accounts
    it "2 deposits occured so should now have 6 bas and 4 accounts" do
      fetching_2_should_now_have_6_bas_and_4_accounts_helper
    end
    it "updates one previous pending ba to confirmed and updates it's"+
      " balances and original account balance" do
      fetching_2_should_change_one_ba_to_confirmed_and_update_balances_helper
    end
    it "updates one previous pending ba to confirmed and doesn't updates"+
      " balances" do
      fetching_2_should_change_one_ba_to_confirmed_and_not_update_balances_helper
    end
    it "creates a new ba and attaches it to a previous account but doesn't"+
      " update balances" do
      fetching_2_should_create_new_ba_with_previous_account_and_not_update_balances_helper
    end
    it "doesn't change other bas" do
      fetching_2_doesnt_update_bas_it_shouldnt_helper
    end
    it "doesn't change other accounts" do
      fetching_2_doesnt_update_accounts_it_shouldnt_helper
    end
  end

  describe "fetch logs third time" do
    # add 2 more logs that are within confirmed block number so
    # they are created and update previous account immediately
    # All balance activities and acocunts should have correct data
    it "has correct number of bas and accounts" do
      fetching_3_should_now_have_8_bas_and_4_accounts_helper
    end
    it "only updates one account and two bas" do
      fetching_3_should_update_one_account_and_two_bas_helper
    end
    it "creates new ba, associates to old account and updates balances" do
      fetching_3_creates_ba_has_old_account_updates_balances_helper
    end
    it "creates new ba, associates to old account and doesnt update balances" do
      fetching_3_creates_ba_has_old_account_doesnt_update_balances_helper
    end
    it "sets activity_type credit_or_debit and gas for all" do
      fetching_3_sets_activity_type_credit_or_debit_and_gas_helper
    end
    it "sets correct created_at_block_number for each account" do
      fetching_3_sets_created_at_block_number_for_accounts_helper
    end
  end

end
