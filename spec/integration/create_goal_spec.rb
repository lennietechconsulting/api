# user can create a goal
# user can't create goal if another goal already has same signature
# user can't create goal if amount is more than usable balance
# user can't create goal if signature is not verified
# after create check to see that this affects their usable balance
#
# can't create goal without account
require 'rails_helper'

RSpec.describe Goal, type: :model do

  # stub Signature.verify_sig(data, sigType)
  describe "creating a goal" do
    it "user can create a goal, address and it updates account balance" do
      goal_user_can_create_goal_helper
    end
  end
end

