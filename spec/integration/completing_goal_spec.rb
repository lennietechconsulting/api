# user can complete a goal
# Expired:
# goal updates as expired and account is charged if goal is expired
# Not Expired:
# goal doesn't update if user is not close enough to location
# goal updates as success and account is not charged if goal is not expired
require 'rails_helper'

RSpec.describe Goal, type: :model do

  describe "completing goal" do
    it "user can submit a goal as complete and user is not charged" do
      goal_completion_updates_goal_and_user_not_charged_helper
    end
    it "if submit when expired, user is charged" do
      goal_completion_updates_expired_goal_and_user_is_charged_helper
    end
  end

end

