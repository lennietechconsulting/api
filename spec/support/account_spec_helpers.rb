module AccountSpecHelpers

  UNFORMATTED_ADDRESS = "0x4a00204770c12e76cc1de10660ab460f8a85a5be"
  FORMATTED_ADDRESS = "4a00204770c12e76cc1de10660ab460f8a85a5be"

  ##################################################
  # ASSOCIATIONS
  ##################################################
  def account_has_many_balance_activities_helper
    account = create(:account_with_balance_activities,
                     balance_activities_count: 3)
    expect(account.balance_activities.count).to eq(3)
  end
  def account_has_many_goals_helper
    account = create(:account)
    goal = create(:goal, account: account)
    expect(account.goals.first).to eq(goal)
  end

  ##################################################
  # VALIDATIONS
  ##################################################
  def account_should_be_valid_helper
    expect(build(:account).valid?).to be true
  end
  def account_eth_address_unique_helper
    account = create(:account)
    expect(build(:account, eth_address: account.eth_address).valid?).to be false
  end
  def account_created_at_block_number_greater_than_0_helper
    expect(build(:account, created_at_block_number: 0).valid?).to be false
  end
  def account_eth_address_is_40_helper
    expect(build(:account, eth_address: "123").valid?).to be false
    expect(build(:account, eth_address: FORMATTED_ADDRESS).valid?).to be true
  end

  ##################################################
  # #SUM_OF_TOTAL_PENDING_CHARGES
  ##################################################
  def account_sum_of_toal_pending_charges_helper
    account = create(:account)
    other_account = create(:account)
    create(:goal, account: account, amount: 1, status: "pending")
    create(:goal, account: other_account, amount: 1, status: "pending")
    create(:goal, account: account, amount: 2, status: "pending")
    create(:goal, account: account, amount: 3, status: "success")
    sum = account.sum_of_total_pending_charges
    expect(sum).to eq(3)
  end

  ##################################################
  # #USABLE_BALANCE
  ##################################################
  def account_usable_balance_returns_correct_account_helper
    account = create(:account, balance: 3)
    allow_any_instance_of(Account).to receive(:sum_of_total_pending_charges)
                .and_return(2)
    result = account.usable_balance
    expect(result).to eq(1)
  end
  def account_usable_balance_calls_sum_of_total_pending_charges_helper
    account = create(:account, balance: 3)
    expect_any_instance_of(Account).to receive(:sum_of_total_pending_charges)
                .and_return(2)
    result = account.usable_balance
    expect(result).to eq(1)
  end

  ##################################################
  # .FIND_BY_ADDRESS
  ##################################################
  def account_find_by_address_calls_returns_correct_account_helper
    account = create(:account)
    result = Account.find_by_address("0x" + account[:eth_address])
    expect(result).to eq(account)
  end
  def account_find_by_address_calls_find_by_with_formatted_address_helper
    account = create(:account)
    expect(Account).to receive(:find_by).with(eth_address: account[:eth_address])
    Account.find_by_address("0x" + account[:eth_address])
  end

  ##################################################
  # .UPDATE_DATA_AND_RETURN_ACCOUNT
  ##################################################
  def account_update_data_and_return_returns_correct_account_info_helper
    account = create(:account)
    expect(Account).to receive(:find_by).with(eth_address: account[:eth_address])
                   .and_return(account)
    result = Account.update_data_and_return_account("0x" + account[:eth_address])
    expect(result[:address]).to eq(account[:eth_address])
    expect(result[:balance]).to eq(account[:balance])
    expect(result[:usable_balance]).to eq(account.usable_balance)
  end
  def account_update_data_and_return_calls_find_by_address_helper
    account = create(:account)
    expect(Account).to receive(:find_by_address)
                   .with("0x" + account[:eth_address])
                   .and_return(account)
    result = Account.update_data_and_return_account("0x" + account[:eth_address])
    expect(result).not_to eq("fail")
  end
  def account_update_data_and_return_calls_goal_update_pending_helper
    account = create(:account)
    create(:goal, account: account)
    expect(Goal).to receive(:update_pending_expired_goals).with(account.goals)
    result = Account.update_data_and_return_account("0x" + account[:eth_address])
    expect(result).not_to eq("fail")
  end
  def account_update_data_and_return_returns_fail_helper
    create(:account)
    result = Account.update_data_and_return_account("adfasd")
    expect(result).to eq("fail")
  end

  ##################################################
  # .VALID_DEPOSITS
  ##################################################
  def account_valid_deposits_returns_correct_deposits_helper
    account = create(:account, eth_address: FORMATTED_ADDRESS)
    expect(account.balance_activities).to receive(:valid_deposits).and_return(0)
    expect(account.valid_deposits).to eq(0)
  end

  ##################################################
  # .FORMAT_ADDRESS
  ##################################################
  def account_format_address_returns_correct_format_helper
    result = Account.format_address(UNFORMATTED_ADDRESS)
    expect(result).to eq(FORMATTED_ADDRESS)
  end

  ##################################################
  # .FIND_OR_CREATE_ACCOUNT
  ##################################################
  def account_info_data_helper
    {
      account_address: UNFORMATTED_ADDRESS,
      block_number: 5
    }
  end
  def account_find_or_create_account_returns_old_account_helper
    info = account_info_data_helper
    create(:account, eth_address: FORMATTED_ADDRESS)
    expect(Account.count).to eq(1)
    expect(Account).not_to receive(:create_account)
    result = Account.find_or_create_account(info)
    expect(Account.count).to eq(1)
    expect(result).to eq(Account.first)
  end
  def account_find_or_create_account_creates_new_account_helper
    info = account_info_data_helper
    expect(Account).to receive(:create_account).with(info).and_return("account")
    result = Account.find_or_create_account(info)
    expect(result).to eq("account")
  end
  def account_find_or_create_account_calls_format_address_helper
    info = account_info_data_helper
    expect(Account).to receive(:format_address).with(info[:account_address])
                   .and_return(FORMATTED_ADDRESS)
    Account.find_or_create_account(info)
  end

  ##################################################
  # .CREATE_ACCOUNT
  ##################################################
  def account_create_account_returns_new_account_helper
    expect(Account.count).to eq(0)
    address = FORMATTED_ADDRESS
    account_info = { account_address: address, block_number: 5 }
    result = Account.create_account(account_info)
    expect(Account.count).to eq(1)
    expect(Account.first).to eq(result)
    expect(Account.first[:eth_address]).to eq(address)
    expect(Account.first[:balance]).to eq(0.0)
    expect(Account.first[:created_at_block_number]).to eq(5)
  end
  def account_create_account_returns_exception_helper
    expect(Account.count).to eq(0)
    expect { Account.create_account({}) }.to raise_error(ActiveRecord::RecordInvalid)
    expect(Account.count).to eq(0)
  end

end
