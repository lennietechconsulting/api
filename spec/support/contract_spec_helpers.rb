module ContractSpecHelpers

  ##################################################
  # TEST STUBS, DATA, DOUBLES
  ##################################################

  module Client
    def self.eth_block_number
      ContractDataHelpers.eth_block_number
    end
    def self.eth_get_logs(params)
      "raw logs"
    end
    def self.eth_get_transaction_receipt(transactionHash)
      ContractDataHelpers.eth_get_transaction_receipt_success
    end
  end

  def contract_stubs_helper
    ENV["NUM_OF_REQUIRED_CONFIRMATIONS"] = "2"
    allow(Ethereum::HttpClient).to receive(:new).with(ENV["CLIENT_ADDRESS"])
                              .and_return(Client)
  end

  ##################################################
  # .GET_VALUES_FROM_ARGS
  ##################################################
  def contract_get_values_from_args_returns_correct_values_when_2_args_helper
    args = ["one", "two"]
    address, amount = Contract.get_values_from_args(args)
    expect(address).to eq("one")
    expect(amount).to eq("two")
  end
  def contract_get_values_from_args_returns_correct_values_when_4_args_helper
    args = ["one", "two", "three", "four"]
    address, amount = Contract.get_values_from_args(args)
    expect(address).to eq("two")
    expect(amount).to eq("four")
  end

  ##################################################
  # .GET_DATA_TYPES
  ##################################################
  def contract_get_data_types_returns_2_types_when_should_helper
    data = "0000000000000000000000004a00204770c12e76cc1de10660ab46"+
           "0f8a85a5be0000000000000000000000000000000000000000000000"+
           "0006f05b59d3b20000"
    expected_results = ["address", "uint256"]
    expect(data.length === 128).to eq(true)
    results = Contract.get_data_types(data)
    expect(results).to eq(expected_results)
  end
  def contract_get_data_types_returns_4_types_when_should_helper
    data = "0000000000000000000000004a00204770c12e76cc1de10660ab46"+
           "0f8a85a5be0000000000000000000000000000000000000000000000"+
           "0006f05b59d3b20000" +
           "0000000000000000000000004a00204770c12e76cc1de10660ab46"+
           "0f8a85a5be0000000000000000000000000000000000000000000000"+
           "0006f05b59d3b20000"
    expected_results = ["address", "address", "uint256", "uint256"]
    expect(data.length / 64).to eq(4)
    results = Contract.get_data_types(data)
    expect(results).to eq(expected_results)
  end

  ##################################################
  # .GET_LOGS
  ##################################################
  def contract_get_logs_stubs_helper
    allow(Contract).to receive(:create_client).and_return(Client)
    allow(Contract).to receive(:get_raw_logs).and_return("logs")
    allow(Contract).to receive(:get_formatted_data_from_raw_logs)
                   .and_return("formatted results")
  end
  def contract_get_logs_calls_get_formatted_data_helper
    contract_get_logs_stubs_helper
    expect(Contract).to receive(:get_formatted_data_from_raw_logs)
                    .with(Client, "logs")
                    .and_return("formatted results")
    results = Contract.get_logs
    expect(results).to eq("formatted results")
  end
  def contract_get_logs_calls_get_raw_logs_helper
    contract_get_logs_stubs_helper
    expect(Contract).to receive(:get_raw_logs).with(Client, {})
                    .and_return("logs")
    Contract.get_logs
  end
  def contract_get_logs_calls_create_client_helper
    contract_get_logs_stubs_helper
    expect(Contract).to receive(:create_client).and_return(Client)
    Contract.get_logs
  end

  ##################################################
  # .GET_CONFIRMATION_STATUS
  ##################################################
  def contract_get_conf_status_returns_pending_helper
    block_number = 6
    min_conf_number = 5
    result = Contract.get_confirmation_status(block_number, min_conf_number)
    expect(result).to eq("pending")
  end
  def contract_get_conf_status_returns_confirmed_helper
    # equal
    block_number = 5
    min_conf_number = 5
    result = Contract.get_confirmation_status(block_number, min_conf_number)
    expect(result).to eq("confirmed")
    # less than
    block_number = 5
    min_conf_number = 6
    result = Contract.get_confirmation_status(block_number, min_conf_number)
    expect(result).to eq("confirmed")
  end

  ##################################################
  # .GET_FORMATTED_DATA_FROM_RAW_LOGS
  ##################################################
  def contract_get_formatted_data_stubs_helper
    allow(Contract).to receive(:get_max_confirmed_block_number).and_return(10)
    allow(Contract).to receive(:get_args_for_transaction).and_return([1,2])
    allow(Contract).to receive(:get_confirmation_status).and_return("confirmed")
  end
  def contract_get_formatted_data_calls_get_values_from_args_helper
    contract_get_formatted_data_stubs_helper
    logs = ContractDataHelpers.eth_get_logs
    expect(Contract).to receive(:get_values_from_args).with([1,2])
    Contract.get_formatted_data_from_raw_logs(Client, logs)
  end
  def contract_get_formatted_data_returns_correct_data_helper
    contract_get_formatted_data_stubs_helper
    expected_results = [{
      :account_address=>1,
      :amount=>2,
      :transaction_hash=>
       "0x7e7e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d",
      :block_number=>8,
      :gas_used=>23145,
      :transaction_status=>1,
      :confirmation_status=>"confirmed"}]
    logs = ContractDataHelpers.eth_get_logs
    results = Contract.get_formatted_data_from_raw_logs(Client, logs)
    expect(results).to eq(expected_results)
  end
  def contract_get_formatted_data_calls_get_confirmation_status_helper
    contract_get_formatted_data_stubs_helper
    logs = ContractDataHelpers.eth_get_logs
    expect(Contract).to receive(:get_confirmation_status)
                    .with(8, 10)
    Contract.get_formatted_data_from_raw_logs(Client, logs)
  end
  def contract_get_formatted_data_calls_get_args_for_transaction_helper
    contract_get_formatted_data_stubs_helper
    logs = ContractDataHelpers.eth_get_logs
    transaction = ContractDataHelpers.eth_get_transaction_receipt_success
    expect(Contract).to receive(:get_args_for_transaction)
                  .with(transaction)
                  .and_return([1,2])
    Contract.get_formatted_data_from_raw_logs(Client, logs)
  end
  def contract_get_formatted_data_calls_get_max_confirmed_block_helper
    contract_get_formatted_data_stubs_helper
    logs = ContractDataHelpers.eth_get_logs
    expect(Contract).to receive(:get_max_confirmed_block_number)
                  .with(Client).and_return(10)
    Contract.get_formatted_data_from_raw_logs(Client, logs)
  end
  def contract_get_formatted_data_calls_eth_get_transaction_receipt_helper
    contract_get_formatted_data_stubs_helper
    logs = ContractDataHelpers.eth_get_logs
    transaction = ContractDataHelpers.eth_get_transaction_receipt_success
    expect(Client).to receive(:eth_get_transaction_receipt)
                  .with(logs["result"][0]["transactionHash"])
                  .and_return(transaction)
    Contract.get_formatted_data_from_raw_logs(Client, logs)
  end

  ##################################################
  # .GET_ARGS_FOR_TRANSACTION
  ##################################################
  def get_args_for_transaction_stubs_helper
    decoder = Ethereum::Decoder.new
    allow(Ethereum::Decoder).to receive(:new).and_return(decoder)
    allow(Contract).to receive(:get_data_types).and_return(["address", "uint256"])
    allow(decoder).to receive(:decode).and_return(true)
    return { decoder: decoder }
  end
  def contract_get_args_for_transaction_uses_first_log_helper
    get_args_for_transaction_stubs_helper
    transaction = {
      "result" => {
        "logs" => [{"data" => "0xdata_one"}]
      }
    }
    expect(Contract).to receive(:get_data_types).with("data_one")
    Contract.get_args_for_transaction(transaction)
  end
  def contract_get_args_for_transaction_uses_second_log_helper
    get_args_for_transaction_stubs_helper
    transaction = {
      "result" => {
        "logs" => [{"data" => "0xdata_one"}, {"data" => "0xdata_two"}]
      }
    }
    expect(Contract).to receive(:get_data_types).with("data_two")
    Contract.get_args_for_transaction(transaction)
  end
  def contract_get_args_for_transaction_calls_get_data_types_helper
    get_args_for_transaction_stubs_helper
    transaction = {
      "result" => {
        "logs" => [{"data" => "0xdata_one"}]
      }
    }
    expect(Contract).to receive(:get_data_types).with("data_one")
    Contract.get_args_for_transaction(transaction)
  end
  def contract_get_args_for_transaction_calls_decode_helper
    stubs = get_args_for_transaction_stubs_helper
    data ="0000000000000000000000004a00204770c12e76cc1de10660ab460f8a85a5be"+
          "00000000000000000000000000000000000000000000000006f05b59d3b20000"

    expect(stubs[:decoder]).to receive(:decode).once.ordered.with("address", data, 0)
                   .and_return(1)
    expect(stubs[:decoder]).to receive(:decode).once.ordered.with("uint256", data, 64)
                   .and_return(2)

    transaction = ContractDataHelpers.eth_get_transaction_receipt_success
    results = Contract.get_args_for_transaction(transaction)
    expect(results).to eq([1,2])
  end

  ##################################################
  # .GET_RAW_LOGS
  ##################################################
  def contract_get_raw_logs_stubs
    return {
      fromBlock: '0x0',
      toBlock: 'latest',
      address: Contract::CONTRACT_ADDRESS,
      topics: []
    }
  end
  def contract_get_raw_logs_calls_default_params
    log_params = contract_get_raw_logs_stubs
    expect(Client).to receive(:eth_get_logs).with(log_params)
    Contract.get_raw_logs(Client)
  end
  def contract_get_raw_logs_calls_topics_default_helper
    contract_get_raw_logs_calls_default_params
  end
  def contract_get_raw_logs_calls_topics_option_helper
    log_params = contract_get_raw_logs_stubs
    log_params[:topics] = ["adf"]
    expect(Client).to receive(:eth_get_logs).with(log_params)
    Contract.get_raw_logs(Client, { topics: ["adf"] })
  end
  def contract_get_raw_logs_calls_with_from_block_default_helper
    contract_get_raw_logs_calls_default_params
  end
  def contract_get_raw_logs_calls_with_from_block_option_helper
    log_params = contract_get_raw_logs_stubs
    log_params[:fromBlock] = 2
    expect(Client).to receive(:eth_get_logs).with(log_params)
    Contract.get_raw_logs(Client, { from_block: 2 })
  end
  def contract_get_raw_logs_calls_with_no_to_block_option_helper
    log_params = contract_get_raw_logs_stubs
    log_params[:toBlock] = 'latest'
    expect(Client).to receive(:eth_get_logs).with(log_params)
    Contract.get_raw_logs(Client)
  end
  def contract_get_raw_logs_calls_with_to_block_option_helper
    log_params = contract_get_raw_logs_stubs
    log_params[:toBlock] = 8
    expect(Client).to receive(:eth_get_logs).with(log_params)
    Contract.get_raw_logs(Client, { to_block: 8 })
  end
  def contract_get_raw_logs_calls_client_eth_get_logs_helper
    log_params = contract_get_raw_logs_stubs
    expect(Client).to receive(:eth_get_logs)
                  .with(log_params).and_return("raw logs")
    results = Contract.get_raw_logs(Client)
    expect(results).to eq("raw logs")
  end

  ##################################################
  # .GET_MAX_CONFIRMED_BLOCK_NUMBER
  ##################################################
  def contract_get_max_conf_block_num_returns_correct_to_block_number_helper
    self.contract_stubs_helper
    ENV["NUM_OF_REQUIRED_CONFIRMATIONS"] = "2"
    expect(Client).to receive(:eth_block_number).and_return({"result"=>"0x0b"})
    result = Contract.get_max_confirmed_block_number(Client)
    expect(result).to eq(9)
  end

  ##################################################
  # .CREATE_CLIENT
  ##################################################
  def contract_create_client_returns_new_client_helper
    self.contract_stubs_helper
    expect(Contract.create_client).to eq(Client)
  end

end
