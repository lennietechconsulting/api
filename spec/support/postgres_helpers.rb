module PostgresTestHelpers

  SHA1 = "ed15c0835e2e6cdc0135e94fdadfee1f70e2974e"

  module BackupTestHelpers
    def pg_stub_backup_helper
      allow(Postgres).to receive(:system)
    end

    def pg_test_backup_calls_log_messages_helper
      pg_stub_backup_helper
      expect(Rails.logger).to receive(:info).with("Dumping database")
      expect(Rails.logger).to receive(:info).with("Done!")
      Postgres.backup
    end

    def pg_test_backup_calls_system_with_proper_command_helper
      pg_stub_backup_helper
      expect(Postgres).to receive(:system).with(
            "PGPASSWORD=#{Postgres::DB_PASSWORD} \
            pg_dump \
            -h #{Postgres::DB_HOST} \
            -p #{Postgres::DB_PORT} \
            -U #{Postgres::DB_USER} \
            -Ft \
            -d #{Postgres::DB_NAME} \
            | gzip > backup_#{Rails.env}.tar.gz"
      )
      Postgres.backup
    end
  end

  module PushBackupTestHelpers

    def pg_stub_push_backup_helper failed=false, corrupt=false
      if failed.present?
        response = { status: 500, message: "Could not upload file" }
      elsif corrupt.present?
        response = { contentSha1: "corrupt" }
      else
        response = { contentSha1: SHA1 }
      end

      allow(B2).to receive(:upload_file).and_return(response)
    end

    def pg_stub_file_exist_helper no_file=false
      if no_file.present?
        allow(File).to receive(:exist?).and_return(false)
      else
        allow(File).to receive(:exist?).and_return(true)
        allow(File).to receive(:read).and_return(File.read("tmp/backup_test.tar.gz"))
      end
    end

    def pg_test_push_backup_calls_proper_upload_method_helper
      pg_stub_push_backup_helper
      expect(B2).to receive(:upload_file).with(
        B2::POSTGRES_BUCKET_ID, "backup_test.tar.gz", "application/x-gzip"
      ).once
      system "touch backup_test.tar.gz"
      Postgres.push_backup_to_b2
    end

    def pg_test_should_not_call_upload_file_helper
      expect(B2).to receive(:upload_file).with(
        B2::POSTGRES_BUCKET_ID, "backup_test.tar.gz", "application/x-gzip"
      ).never
      Postgres.push_backup_to_b2
    end

    def pg_test_should_call_log_with_error_helper
      expect(Rails.logger).to receive(:error).with("Postgres backup file with path backup_test.tar.gz doesn't exist").once
      Postgres.push_backup_to_b2
    end

    def pg_test_log_error_when_push_fails_helper
   expect(Rails.logger).to receive(:error).with("Error pushing postgres backup to B2").once
      system "touch backup_test.tar.gz"
      Postgres.push_backup_to_b2
    end

    def pg_test_push_backup_succeeds_logs_info_helper
      expect(Rails.logger).to receive(:info).with("Pushing postgres backup to B2").once
      expect(Rails.logger).to receive(:info).with("Removing backup gzip file").once
      expect(Rails.logger).to receive(:info).with("Done!").once
      system "touch backup_test.tar.gz"
      Postgres.push_backup_to_b2
    end

    def pg_test_backup_file_removed_helper
      expect(Postgres).to receive(:system).with("rm backup_test.tar.gz").once
      system "touch backup_test.tar.gz"
      Postgres.push_backup_to_b2
    end

    def pg_test_backup_success_should_not_have_error_log
      expect(Rails.logger).to receive(:error).with("Error pushing postgres backup to B2").never
      system "touch backup_test.tar.gz"
      Postgres.push_backup_to_b2
    end
  end

  module PullBackupTestHelpers

    def pg_delete_backup_file_if_exists_helper
      file = "backup_test.tar.gz"
      if File.exist?(file)
        File.delete(file)
      end
    end

    def pg_download_success_headers_helper corrupt=false
      sha1 = corrupt.present? ? "corrupt" : SHA1

      {"server"=>["Apache-Coyote/1.1"], "x-content-type-options"=>["nosniff"], "x-xss-protection"=>["1; mode=block"], "x-frame-options"=>["SAMEORIGIN"], "cache-control"=>["max-age=0, no-cache, no-store"], "x-bz-file-name"=>["tmp/backup_test"], "x-bz-file-id"=>["4_zedefa07f713b96ae57230f1d_f115ff518ae83411a_d20160207_m022640_c001_v0001005_t0033"], "x-bz-content-sha1"=>[sha1], "accept-ranges"=>["bytes"], "content-type"=>["application/x-gzip"], "content-length"=>["7678"], "date"=>["Wed, 10 Feb 2016 04:30:33 GMT"], "connection"=>["close"]}
    end

    def pg_stub_download_file_by_name_helper failed=false, corrupt=false
      if failed.present?
        response = { status: 500, message: "Download file failed" }
      else
        response = {
          headers: pg_download_success_headers_helper(corrupt.present?),
          body: File.read("tmp/backup_test.tar.gz").force_encoding("ISO-8859-1")
        }
      end

      allow(B2).to receive(:download_file_by_name).and_return(response)
    end

    def pg_test_should_call_download_file_by_name_helper
      pg_stub_download_file_by_name_helper
      expect(B2).to receive(:download_file_by_name).with(
        Postgres::BUCKET_NAME,
        "backup_test").once
      Postgres.pull_backup_from_b2
    end

    def pg_test_download_file_should_have_logger_message_helper
      pg_stub_download_file_by_name_helper
      expect(Rails.logger).to receive(:info).with("Pulling postgres backup from B2").once
      expect(Rails.logger).to receive(:info).with("Done!").once
      Postgres.pull_backup_from_b2
    end

    def pg_test_logger_error_when_download_fails_helper
      pg_stub_download_file_by_name_helper "fail"
      expect(Rails.logger).to receive(:error).with("Error pulling postgres backup from B2").once
      Postgres.pull_backup_from_b2
    end

    def pg_test_download_fail_doesnt_create_file_helper
      pg_stub_download_file_by_name_helper "fail"
      expect(File).to receive(:open).never
      Postgres.pull_backup_from_b2
    end

    def pg_test_should_call_file_open_helper
      pg_stub_download_file_by_name_helper
      expect(File).to receive(:open).with("backup_test.tar.gz", "wb+")
      Postgres.pull_backup_from_b2
    end

    def pg_test_backup_file_should_be_created_helper
      pg_stub_download_file_by_name_helper
      expect(File.exist?("backup_test.tar.gz")).to be false
      Postgres.pull_backup_from_b2
      expect(File.exist?("backup_test.tar.gz")).to be true
    end
  end

  module RestoreTestHelpers
    def restore_command_helper
            "PGPASSWORD=#{Postgres::DB_PASSWORD} \
            pg_restore \
            -h #{Postgres::DB_HOST} \
            -p #{Postgres::DB_PORT} \
            -U #{Postgres::DB_USER} \
            -d #{Postgres::DB_NAME} \
            backup_#{Rails.env}.tar"
    end

    def pg_stub_restore_helper
      allow(Postgres).to receive(:system)
    end

    def pg_stub_restore_file_exist_helper
      allow(File).to receive(:exist?).and_return(true)
    end

    def pg_test_restore_calls_proper_logger_messages_helper
      pg_stub_restore_helper
      pg_stub_restore_file_exist_helper
      expect(Rails.logger).to receive(:info).with("Uncompressing gzip file")
      expect(Rails.logger).to receive(:info).with("Restoring database")
      expect(Rails.logger).to receive(:info).with("Removing backup file")
      expect(Rails.logger).to receive(:info).with("Done!")
      Postgres.restore
    end

    def pg_test_restore_checks_for_file_helper
      pg_stub_restore_helper
      expect(File).to receive(:exist?).with("backup_#{Rails.env}.tar.gz")
      Postgres.restore
    end

    def pg_test_restore_calls_error_when_no_file_helper
      pg_stub_restore_helper
      expect(Rails.logger).to receive(:error).with("No backup file to restore")
      Postgres.restore
    end
    def pg_test_restore_doesnt_call_restore_when_no_file_helper
      pg_stub_restore_helper
      expect(Postgres).to receive(:system).with(restore_command_helper).never
      Postgres.restore
    end
    def pg_test_unzip_file_called_helper
      pg_stub_restore_helper
      pg_stub_restore_file_exist_helper
      expect(Postgres).to receive(:system).with("gzip -d backup_#{Rails.env}.tar.gz").once
      Postgres.restore
    end

    def pg_test_restore_called_helper
      pg_stub_restore_helper
      pg_stub_restore_file_exist_helper
      expect(Postgres).to receive(:system).with(restore_command_helper).once
      Postgres.restore
    end

    def pg_restore_file_removed_helper
      pg_stub_restore_helper
      pg_stub_restore_file_exist_helper
      expect(Postgres).to receive(:system).with("rm backup_#{Rails.env}.tar").once
      Postgres.restore
    end
  end

  def pg_stub_backup_and_push_helper
    allow(File).to receive(:exist?).and_return(false)
    allow(File).to receive(:delete).and_return(nil)
    allow(Postgres).to receive(:system).and_return(nil)
    allow(Postgres).to receive(:backup).and_return(nil)
    allow(Postgres).to receive(:push_backup_to_b2).and_return(nil)
  end

  def pg_stub_pull_and_restore_helper
    allow(File).to receive(:exist?).and_return(false)
    allow(File).to receive(:delete).and_return(nil)
    allow(Postgres).to receive(:system).and_return(nil)
    allow(Postgres).to receive(:pull_backup_from_b2).and_return(nil)
    allow(Postgres).to receive(:restore).and_return(nil)
  end

  def pg_test_backup_and_push_calls_backup_helper
    pg_stub_backup_and_push_helper
    expect(Postgres).to receive(:backup).once
    Postgres.backup_and_push_to_b2
  end

  def pg_test_backup_and_push_calls_push_backup_helper
    pg_stub_backup_and_push_helper
    expect(Postgres).to receive(:push_backup_to_b2).once
    Postgres.backup_and_push_to_b2
  end

  def pg_test_pull_and_restore_calls_pull_helper
    pg_stub_pull_and_restore_helper
    expect(Postgres).to receive(:pull_backup_from_b2).once
    Postgres.pull_from_b2_and_restore
  end

  def pg_test_pull_and_restore_calls_restore_helper
    pg_stub_pull_and_restore_helper
    expect(Postgres).to receive(:restore).once
    Postgres.pull_from_b2_and_restore
  end

end
