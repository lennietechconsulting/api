module ContractDataHelpers

  def self.eth_block_number(number=nil)
    result = number || "0x0b" #"0x0b" is 11
    {"id"=>1, "jsonrpc"=>"2.0", "result"=>result}
  end
  # Assuming that current_block number is 11
  #  and num of required confirmations is 1
  def self.formatted_logs
    [{:account_address=>"4a00204770c12e76cc1de10660ab460f8a85a5be",
      :amount=>500000000000000000,
      :transaction_hash=>
       "0x7e7e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d",
      :block_number=>8,
      :gas_used=>23145,
      :transaction_status=>"0x1",
      :confirmation_status=>"confirmed"},
     {:account_address=>"4a00204770c12e76cc1de10660ab460f8a85a5be",
      :amount=>600000000000000000,
      :transaction_hash=>
       "0x8ff877ff0999dfb80f5dc534283b23c5412f789f2b6ff77a25f1babee1f0e442",
      :block_number=>9,
      :gas_used=>23145,
      :transaction_status=>"0x1",
      :confirmation_status=>"confirmed"},
     {:account_address=>"4a00204770c12e76cc1de10660ab460f8a85a5be",
      :amount=>130000000000000000,
      :transaction_hash=>
       "0x4d6b7c065d750e934f778d84a190b73398796820342f6a69dbfc8c62920e90bf",
      :block_number=>10,
      :gas_used=>23145,
      :transaction_status=>"0x1",
      :confirmation_status=>"confirmed"},
     {:account_address=>"4a00204770c12e76cc1de10660ab460f8a85a5be",
      :amount=>140000000000000000,
      :transaction_hash=>
       "0x1c1748f87bf6ec26b9c1b79d6a1ceb823242ac845d4fdf74620617af43b0024f",
      :block_number=>11,
      :gas_used=>23145,
      :transaction_status=>"0x1",
      :confirmation_status=>"pending"}]
  end
  def self.eth_get_logs
    {"id"=>1,
     "jsonrpc"=>"2.0",
     "result"=>
      [{"logIndex"=>"0x00",
        "transactionIndex"=>"0x00",
        "transactionHash"=>
         "0x7e7e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d",
        "blockHash"=>
         "0x1dd969905023810c6b1f5408978e57745fe1ca21182436b79ade06e50867ef37",
        "blockNumber"=>"0x08",
        "address"=>"0x773d96be0c547e1b4619409936b478bd64839628",
        "data"=>
         "0x0000000000000000000000004a00204770c12e76cc1de10660ab460f8a85a5be00000000000000000000000000000000000000000000000006f05b59d3b20000",
        "topics"=>
         ["0x1de3ece35391f6bba650736f2d5d3f12e2cfc54538a340cc5d762d34ca449de7",
          "0x0000000000000000000000004a00204770c12e76cc1de10660ab460f8a85a5be"],
        "type"=>"mined"}]}
  end
  def self.eth_get_transaction_receipt_fail
    transaction = self.eth_get_transaction_receipt_success
    transaction["result"]["status"] = "0x0"
    transaction
  end
  def self.eth_get_transaction_receipt_success
    {"id"=>1,
     "jsonrpc"=>"2.0",
     "result"=>
      {"transactionHash"=>
        "0x7e7e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d",
       "transactionIndex"=>"0x00",
       "blockHash"=>
        "0x1dd969905023810c6b1f5408978e57745fe1ca21182436b79ade06e50867ef37",
       "blockNumber"=>"0x08",
       "gasUsed"=>"0x5a69",
       "cumulativeGasUsed"=>"0x5a69",
       "contractAddress"=>nil,
       "logs"=>
        [{"logIndex"=>"0x00",
          "transactionIndex"=>"0x00",
          "transactionHash"=>
           "0x7e7e2e69e4fe9a2b5f29ae5a9bf58f8d38c78b97ebaba97eba47c9b513a0864d",
          "blockHash"=>
           "0x1dd969905023810c6b1f5408978e57745fe1ca21182436b79ade06e50867ef37",
          "blockNumber"=>"0x08",
          "address"=>"0x773d96be0c547e1b4619409936b478bd64839628",
          "data"=>
           "0x0000000000000000000000004a00204770c12e76cc1de10660ab460f8a85a5be00000000000000000000000000000000000000000000000006f05b59d3b20000",
          "topics"=>
           ["0x1de3ece35391f6bba650736f2d5d3f12e2cfc54538a340cc5d762d34ca449de7",
            "0x0000000000000000000000004a00204770c12e76cc1de10660ab460f8a85a5be"],
          "type"=>"mined"}],
       "status"=>"0x1"}}
  end
end
