module CommonSpecHelpers
  def common_validates_presence_for_fields_helper(model, fields, options=nil)
    fields.each do |field|
      if options && field != options[:field]
        ba = build(model, field => nil, options[:field] => options[:value])
        expect(ba.valid?).to eq(false)
      else
        expect(build(model, field => nil).valid?).to be false
      end
    end
  end
  def common_verify_enum_value_helper(model, enum, value, index)
    new_record = build(model, enum => index)
    expect(new_record.valid?).to be true
    expect(new_record[enum]).to eq(value.to_s)
  end
  def common_confirm_enums_with_values_helper(model, enums_with_values)
    enums_with_values.each do |enum|
      key = enum.keys.first
      enum[key].each_with_index do |value, index|
        common_verify_enum_value_helper(model, key, value, index)
      end
    end
  end
end

