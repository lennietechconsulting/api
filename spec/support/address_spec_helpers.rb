module AddressSpecHelpers
  ##################################################
  # VALIDATIONS
  ##################################################
  def address_should_be_valid_helper
    goal = create(:goal)
    address = build(:address, addressable: goal)
    expect(address.valid?).to eq(true)
  end

  ##################################################
  # ASSOCIATIONS
  ##################################################
  def address_has_one_addressable_helper
    goal = create(:goal)
    address = create(:address, addressable: goal)
    expect(address.addressable).to eq(goal)
  end

end
