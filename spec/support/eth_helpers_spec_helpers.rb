module EthHelpersSpecHelpers

  ##################################################
  # TO_WEI
  ##################################################
  def eth_helpers_to_wei_returns_correct_amount_helper
    result = EthHelpers.to_wei("1.0")
    expect(result).to eq(1000000000000000000)
  end

  ##################################################
  # TO_ETHER
  ##################################################
  def eth_helpers_to_ether_returns_correct_amount_helper
    result = EthHelpers.to_ether("1000000000000000000.0")
    expect(result).to eq(1.0)
  end

end
