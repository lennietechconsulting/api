module GoalSpecHelpers

  ##################################################
  # .CREATE_GOAL
  ##################################################
  def goal_create_goal_creates_goal_address_with_correct_data_helper
    start_time_unix = Time.now.to_i.to_s
    end_time_unix = (Time.now + 30.seconds).to_i.to_s
    account = create(:account)
    data = {
      signature: "signature",
      amount_in_wei: "1.1",
      startTimeHuman: "start time",
      startTimeUnix: start_time_unix,
      endTimeHuman: "end time",
      endTimeUnix: end_time_unix,
      account: account,
      latitude: 43.233,
      longitude: 52.111,
      formattedAddress: "address"
    }
    allow(Goal).to receive(:add_extras_to_data).with(data).and_return(data)
    allow(Goal).to receive(:validate_create_goal_data).and_return(true)

    expect(Goal.count).to eq(0)
    expect(Address.count).to eq(0)

    result = Goal.create_goal(data)

    expect(Goal.count).to eq(1)
    expect(Address.count).to eq(1)
    goal = Goal.first
    address = Address.first

    expect(goal[:signature]).to eq("signature")
    expect(goal[:amount]).to eq(1.1)
    expect(goal[:start_time_human]).to eq("start time")
    expect(goal[:start_time_unix]).to eq(start_time_unix.to_i)
    expect(goal[:end_time_human]).to eq("end time")
    expect(goal[:end_time_unix]).to eq(end_time_unix.to_i)
    expect(goal[:status]).to eq("pending")
    expect(goal[:goal_type]).to eq("location_arrival")
    expect(goal.account).to eq(account)

    expect(address[:latitude]).to eq(43.233)
    expect(address[:longitude]).to eq(52.111)
    expect(address[:formatted_address]).to eq("address")
    expect(address.addressable).to eq(goal)

    expect(result).to eq(true)
  end
  def goal_create_goal_returns_false_if_goal_data_not_valid_helper
    data = {}
    allow(Goal).to receive(:add_extras_to_data).with(data).and_return(data)
    allow(Goal).to receive(:validate_create_goal_data).and_return(false)
    result = Goal.create_goal(data)
    expect(result).to eq(false)
  end
  def goal_create_goal_calls_add_extras_helper
    data = {}
    expect(Goal).to receive(:add_extras_to_data).with(data).and_return(data)
    allow(Goal).to receive(:validate_create_goal_data).and_return(false)
    Goal.create_goal(data)
  end

  ##################################################
  # .ADD_EXTRAS_TO_DATA
  ##################################################
  def goal_add_extras_to_data_returns_correct_data_helper
    data = { amount: 1, coinbase: "coinbase" }
    allow(EthHelpers).to receive(:to_wei).and_return(10)
    allow(Account).to receive(:find_by_address).with("coinbase")
                  .and_return("account")
    result = Goal.add_extras_to_data(data)
    expect(result[:amount]).to eq(1)
    expect(result[:coinbase]).to eq("coinbase")
    expect(result[:amount_in_wei]).to eq(10)
    expect(result[:account]).to eq("account")
  end
  def goal_add_extras_to_data_adds_account_to_data_helper
    data = { amount: 1, coinbase: "coinbase" }
    allow(EthHelpers).to receive(:to_wei).and_return(10)
    allow(Account).to receive(:find_by_address).with("coinbase")
                  .and_return("account")
    result = Goal.add_extras_to_data(data)
    expect(result[:amount]).to eq(1)
    expect(result[:coinbase]).to eq("coinbase")
    expect(result[:amount_in_wei]).to eq(10)
    expect(result[:account]).to eq("account")
  end
  def goal_add_extras_to_data_calls_find_by_address_helper
    data = { amount: 1, coinbase: "coinbase" }
    allow(EthHelpers).to receive(:to_wei).and_return(10)
    expect(Account).to receive(:find_by_address).with("coinbase")
                   .and_return("account")
    Goal.add_extras_to_data(data)
  end
  def goal_add_extras_to_data_adds_amount_in_wei_helper
    data = { amount: 1, coinbase: "coinbase" }
    allow(EthHelpers).to receive(:to_wei).and_return(10)
    result = Goal.add_extras_to_data(data)
    expect(result[:amount]).to eq(1)
    expect(result[:coinbase]).to eq("coinbase")
    expect(result[:amount_in_wei]).to eq(10)
  end
  def goals_calls_ethhelpers_to_wei_helper
    data = { amount: 1, coinbase: "coinbase" }
    expect(EthHelpers).to receive(:to_wei).with(1)
    Goal.add_extras_to_data(data)
  end

  ##################################################
  # .VALIDATE_CREATE_GOAL_DATA
  ##################################################
  def goal_validate_create_goal_data_returns_true_if_amount_not_too_much_helper
    account = create(:account)
    data = { signature: "sig", account: account, amount_in_wei: 4 }
    allow(account).to receive(:usable_balance).and_return(5)
    allow(Goal).to receive(:find_by).with(signature: "sig").and_return(false)
    allow(Signature).to receive(:verify_sig).with(data, "goalCreate")
                     .and_return(true)
    result = Goal.validate_create_goal_data(data)
    expect(result).to eq(true)
  end
  def goal_validate_create_goal_data_returns_false_if_amount_too_much_helper
    account = create(:account)
    data = { signature: "sig", account: account, amount_in_wei: 6 }
    allow(account).to receive(:usable_balance).and_return(5)
    allow(Goal).to receive(:find_by).with(signature: "sig").and_return(false)
    allow(Signature).to receive(:verify_sig).with(data, "goalCreate")
                     .and_return(true)
    result = Goal.validate_create_goal_data(data)
    expect(result).to eq(false)
  end
  def goal_validate_create_goal_data_returns_false_if_no_account_helper
    data = { signature: "sig" }
    allow(Goal).to receive(:find_by).with(signature: "sig").and_return(false)
    allow(Signature).to receive(:verify_sig).with(data, "goalCreate")
                     .and_return(true)
    result = Goal.validate_create_goal_data(data)
    expect(result).to eq(false)
  end
  def goal_validate_create_goal_data_returns_false_sig_not_valid_helper
    data = { signature: "sig" }
    allow(Goal).to receive(:find_by).with(signature: "sig").and_return(false)
    expect(Signature).to receive(:verify_sig).with(data, "goalCreate")
                     .and_return(false)
    result = Goal.validate_create_goal_data(data)
    expect(result).to eq(false)
  end
  def goal_validate_create_goal_data_returns_false_if_already_sig_helper
    data = { signature: "sig" }
    expect(Goal).to receive(:find_by).with(signature: "sig").and_return(true)
    expect(Signature).not_to receive(:verify_sig)
    result = Goal.validate_create_goal_data(data)
    expect(result).to eq(false)
  end

  ##################################################
  # .ATTEMPT_TO_UPDATE_AS_COMPLETED
  ##################################################
  def goal_attempt_to_update_as_completed_returns_true_if_not_pending_helper
    params = "params"
    goal = create(:goal, status: "success")
    allow(Goal).to receive(:verify_goal_completion_and_return_goal)
      .with(params).and_return(goal)
    expect_any_instance_of(Goal).to receive(:update_as_success_or_fail_if_should)
                                .with(params)
    result = Goal.attempt_to_update_as_completed(params)
    expect(result).to eq(true)
  end

  def goal_attempt_to_update_as_completed_returns_false_if_still_pending_helper
    params = "params"
    goal = create(:goal, status: "pending")
    allow(Goal).to receive(:verify_goal_completion_and_return_goal)
      .with(params).and_return(goal)
    expect_any_instance_of(Goal).to receive(:update_as_success_or_fail_if_should)
                                .with(params)
    result = Goal.attempt_to_update_as_completed(params)
    expect(result).to eq(false)
  end
  def goal_attempt_to_update_as_completed_calls_update_as_success_or_fail_helper
    params = "params"
    goal = create(:goal, status: "pending")
    allow(Goal).to receive(:verify_goal_completion_and_return_goal)
      .with(params).and_return(goal)
    expect_any_instance_of(Goal).to receive(:update_as_success_or_fail_if_should)
                                .with(params)
    result = Goal.attempt_to_update_as_completed(params)
    expect(result).to eq(false)
  end
  def goal_attempt_to_update_as_completed_returns_false_if_no_goal_helper
    params = "params"
    allow(Goal).to receive(:verify_goal_completion_and_return_goal)
      .with(params).and_return(false)
    expect_any_instance_of(Goal).not_to receive(:update_as_success_or_fail_if_should)
    result = Goal.attempt_to_update_as_completed(params)
    expect(result).to eq(false)
  end
  def goal_attempt_to_update_as_completed_calls_verify_goal_helper
    params = "params"
    expect(Goal).to receive(:verify_goal_completion_and_return_goal)
      .with(params).and_return(false)
    expect_any_instance_of(Goal).not_to receive(:update_as_success_or_fail_if_should)
    Goal.attempt_to_update_as_completed(params)
  end

  ##################################################
  # .VERIFTY_GOAL_COMPLETION_AND_RETURN_GOAL
  ##################################################
  def goal_verify_goal_completion_returns_goal_helper
    goal = create(:goal, status: "pending")
    params = { goalId: goal[:id], coinbase: "0x" + goal.account[:eth_address] }
    allow(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    allow(Account).to receive(:format_address)
      .and_return(goal.account[:eth_address])
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(goal)
  end
  def goal_verify_goal_completion_returns_false_if_coinbase_doesnt_match_helper
    goal = create(:goal, status: "pending")
    params = { goalId: goal[:id], coinbase: "coinbase"}
    allow(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    allow(Account).to receive(:format_address)
                   .and_return("coinbase")
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end
  def goal_verify_goal_completion_calls_account_format_address_helper
    goal = create(:goal, status: "pending")
    params = { goalId: goal[:id], coinbase: "coinbase" }
    allow(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    expect(Account).to receive(:format_address).with("coinbase")
                   .and_return("coinbase")
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end
  def goal_verify_goal_completion_returns_false_if_goal_not_pending_helper
    goal = create(:goal, status: "success")
    params = { goalId: goal[:id] }
    allow(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    expect(Account).not_to receive(:format_address)
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end
  def goal_verify_goal_completion_returns_false_if_no_goal_helper
    params = { goalId: 1 }
    expect(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    expect(Account).not_to receive(:format_address)
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end
  def goal_verify_goal_completion_calls_goal_find_helper
    params = { goalId: 1 }
    expect(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(true)
    expect(Goal).to receive(:find_by).with(id: 1)
    expect(Account).not_to receive(:format_address)
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end
  def goal_verify_goal_completion_returns_false_if_sig_not_verified_helper
    params = "params"
    expect(Signature).to receive(:verify_sig).with(params, "goalFinish")
                     .and_return(false)
    expect(Goal).not_to receive(:find_by)
    expect(Account).not_to receive(:format_address)
    result = Goal.verify_goal_completion_and_return_goal(params)
    expect(result).to eq(false)
  end

  ##################################################
  # .UPDATE_AS_SUCCESS_OR_FAIL_IF_SHOULD
  ##################################################
  def goal_update_as_success_or_fail_calls_update_status_with_success_helper
    goal = create(:goal, status: "pending",
                         start_time_unix: (Time.now - 20.seconds).to_i,
                         end_time_unix: (Time.now + 10.seconds).to_i)
    create(:address, addressable: goal)
    allow(Location).to receive(:close_enough).with(goal.address, "params")
                    .and_return(true)
    expect_any_instance_of(Goal).to receive(:update_status_and_charge_if_should)
                                .with("success")
    result = goal.update_as_success_or_fail_if_should("params")
    expect(result).to eq(nil)
  end
  def goal_update_as_success_or_fail_calls_update_status_with_failed_helper
    goal = create(:goal, status: "pending",
                         start_time_unix: (Time.now - 20.seconds).to_i,
                         end_time_unix: (Time.now - 10.seconds).to_i)
    create(:address, addressable: goal)
    allow(Location).to receive(:close_enough).with(goal.address, "params")
                    .and_return(true)
    expect_any_instance_of(Goal).to receive(:update_status_and_charge_if_should)
                                .with("failed")
    result = goal.update_as_success_or_fail_if_should("params")
    expect(result).to eq(nil)
  end
  def goal_update_as_success_or_fail_returns_nil_if_not_close_enough_helper
    goal = create(:goal, status: "pending")
    create(:address, addressable: goal)
    expect(Location).to receive(:close_enough).with(goal.address, "params")
                    .and_return(false)
    expect_any_instance_of(Goal).not_to receive(:update_status_and_charge_if_should)
    result = goal.update_as_success_or_fail_if_should("params")
    expect(result).to eq(nil)
  end
  def goal_update_as_success_or_fail_returns_nil_if_not_pending_helper
    expect(Location).not_to receive(:close_enough)
    expect_any_instance_of(Goal).not_to receive(:update_status_and_charge_if_should)
    goal = create(:goal, status: "success")
    result = goal.update_as_success_or_fail_if_should("params")
    expect(result).to eq(nil)
    goal_2 = create(:goal, status: "failed")
    result = goal_2.update_as_success_or_fail_if_should("params")
    expect(result).to eq(nil)
  end

  ##################################################
  # .UPDATE_PENDING_EXPIRED_GOALS
  ##################################################
  def goal_update_pending_expired_goals_calls_update_status_and_charge_helper
    create(:goal, status: "pending")
    create(:goal, status: "success")
    goals_to_filter = Goal.where(status: "pending")
    allow(goals_to_filter).to receive(:get_newly_expired_and_pending)
                           .and_return(goals_to_filter)
    expect_any_instance_of(Goal)
                           .to receive(:update_status_and_charge_if_should)
      .with("failed").once
    Goal.update_pending_expired_goals(goals_to_filter)
  end
  def goal_update_pending_expired_goals_calls_get_newly_expired_helper
    create(:goal, status: "pending")
    create(:goal, status: "success")
    goals_to_filter = Goal.where(status: "pending")
    expect(goals_to_filter).to receive(:get_newly_expired_and_pending)
                           .and_return(goals_to_filter)
    Goal.update_pending_expired_goals(goals_to_filter)
  end

  ##################################################
  # .UPDATE_STATUS_AND_CHARGE_IF_SHOULD
  ##################################################
  def goal_update_status_and_charge_doesnt_call_create_charge_helper
    goal = create(:goal, status: "pending")
    expect(BalanceActivity).not_to receive(
      :create_charge_ba_and_charge_account_for_goal_fail).with(goal)
    goal.update_status_and_charge_if_should("success")
  end
  def goal_update_status_and_charge_calls_create_charge_helper
    goal = create(:goal, status: "pending")
    expect(BalanceActivity).to receive(
      :create_charge_ba_and_charge_account_for_goal_fail).with(goal)
    goal.update_status_and_charge_if_should("failed")
  end
  def goal_update_status_and_charge_updates_status_helper
    goal = create(:goal, status: "pending")
    expect(goal[:status]).to eq("pending")
    goal.update_status_and_charge_if_should("success")
    expect(goal[:status]).to eq("success")
  end
  def goal_update_status_and_charge_returns_nil_if_not_pending_helper
    goal = create(:goal, status: "success")
    result = goal.update_status_and_charge_if_should("success")
    expect(result).to eq(nil)
  end

  ##################################################
  # .GET_NEWLY_EXPIRED_AND_PENDING
  ##################################################
  def goals_get_newly_expired_and_pending_includes_account_helper
    expect(Goal).to receive(:includes).with(:account).and_return(Goal)
    Goal.get_newly_expired_and_pending
  end
  def goals_get_newly_expired_and_pending_returns_correct_goals_helper
    goal = create(:goal, status: "pending",
                  start_time_unix: (Time.now - 20.seconds).to_i,
                  end_time_unix: (Time.now - 10.seconds).to_i)
    create(:goal, status: "pending",
                  start_time_unix: (Time.now - 20.seconds).to_i,
                  end_time_unix: (Time.now + 10.seconds).to_i)
    create(:goal, status: "success",
                  start_time_unix: (Time.now - 20.seconds).to_i,
                  end_time_unix: (Time.now - 10.seconds).to_i)
    create(:goal, status: "success",
                  start_time_unix: (Time.now - 20.seconds).to_i,
                  end_time_unix: (Time.now + 10.seconds).to_i)

    goals = Goal.get_newly_expired_and_pending
    expect(goals.count).to eq(1)
    expect(goals.first).to eq(goal)
  end

  ##################################################
  # .UPDATE_ALL_PENDING_EXPIRED_GOALS
  ##################################################
  def goals_update_all_pending_expired_goals_calls_update_pending_with_self_helper
    expect(Goal).to receive(:update_pending_expired_goals).with(Goal)
                .and_return("result")
    result = Goal.update_all_pending_expired_goals
    expect(result).to eq("result")
  end

  ##################################################
  # .GET_LIST_FOR_ETH_ADDRESS
  ##################################################
  def goal_get_list_for_eth_address_returns_correct_info_helper
    account = create(:account)
    other_account = create(:account)
    goal = create(:goal, account: account)
    goal_2 = create(:goal, account: account)
    goal_3 = create(:goal, account: other_account)
    create(:address, addressable: goal)
    create(:address, addressable: goal_2)
    create(:address, addressable: goal_3)
    result = Goal.get_list_for_eth_address("0x" + account.eth_address)
    expected_result = {:json=>
  [{:id=> goal[:id],
    :amount=> goal[:amount],
    :startTime=> goal[:start_time_human],
    :endTime=> goal[:end_time_human],
    :status=> goal[:status],
    :latitude=> goal.address[:latitude],
    :longitude=> goal.address[:longitude],
    :formattedAddress=> goal.address[:formatted_address]},
   {:id=>goal_2[:id],
    :amount=> goal_2[:amount],
    :startTime=> goal_2[:start_time_human],
    :endTime=> goal_2[:end_time_human],
    :status=> goal_2[:status],
    :latitude=> goal_2.address[:latitude],
    :longitude=> goal_2.address[:longitude],
    :formattedAddress=> goal_2.address[:formatted_address]}],
 :status=>200}
    expect(result).to eq(expected_result)
  end
  def goal_get_list_for_eth_address_returns_400_if_no_account_helper
    address = "1234"
    result = Goal.get_list_for_eth_address(address)
    expected_result = { status: 400 }
    expect(result).to eq(expected_result)
  end
  def goal_get_list_for_eth_address_calls_find_by_address_helper
    address = "1234"
    expect(Account).to receive(:find_by_address).with(address)
    Goal.get_list_for_eth_address(address)
  end

  ##################################################
  # ASSOCIATIONS
  ##################################################
  def goal_has_one_address_helper
    goal = create(:goal)
    address = create(:address, addressable: goal)
    expect(goal.address).to eq(address)
  end
  def goal_belongs_to_account_helper
    account = create(:account)
    goal = create(:goal, account: account)
    expect(goal.account).to eq(account)
  end
  def goal_has_many_balance_activities_as_itemable_helper
    goal = create(:goal)
    another_goal = create(:goal)
    ba = create(:balance_activity, itemable: goal)
    ba_2 = create(:balance_activity, itemable: goal)
    ba_3 = create(:balance_activity, itemable: goal)
    ba_4 = create(:balance_activity, itemable: another_goal)
    expect(goal.balance_activities.count).to eq(3)
    expect(goal.balance_activities.include?(ba)).to eq(true)
    expect(goal.balance_activities.include?(ba_2)).to eq(true)
    expect(goal.balance_activities.include?(ba_3)).to eq(true)
    expect(goal.balance_activities.include?(ba_4)).to eq(false)
  end
  ##################################################
  # VALIDATIONS
  ##################################################
  def goal_custom_validation_end_time_greater_than_start_time_helper
    goal = build(:goal, start_time_unix: Time.now.to_i,
                        end_time_unix: (Time.now - 1.seconds).to_i)
    expect(goal.valid?).to be false
    goal_2 = build(:goal, start_time_unix: Time.now.to_i,
                        end_time_unix: (Time.now + 1.seconds).to_i)
    expect(goal_2.valid?).to be true
  end
  def goal_custom_validation_start_time_greater_than_30_sec_helper
    goal = build(:goal, start_time_unix: (Time.now - 40.seconds).to_i)
    expect(goal.valid?).to be false
    goal_2 = build(:goal, start_time_unix: (Time.now - 20.seconds).to_i)
    expect(goal_2.valid?).to be true
  end
  def goal_signature_unique_helper
    create(:goal, signature: "1234")
    goal = build(:goal, signature: "1234")
    expect(goal.valid?).to be false
    goal_2 = build(:goal, signature: "2345")
    expect(goal_2.valid?).to be true
  end
  def goal_amount_greater_than_0_helper
    goal = build(:goal, amount: 0)
    expect(goal.valid?).to be false
    goal_2 = build(:goal, amount: 0.1)
    expect(goal_2.valid?).to be true
  end
  def goal_should_be_valid_helper
    goal = build(:goal)
    expect(goal.valid?).to be true
  end

end
