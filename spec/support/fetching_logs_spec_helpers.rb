module FetchingLogsSpecHelper

  module Client
    def self.eth_get_logs(params)
      true
    end
    def self.eth_block_number
      true
    end
    def self.eth_get_transaction_receipt(hash)
      true
    end
  end
  ##################################################
  # THIRD FETCH
  ##################################################
  def fetch_logs_3_times_helper
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    fetching_stubs_for_second_fetch_helper
    BalanceActivity.update_deposits_from_logs
    fetching_stubs_for_third_fetch_helper
    BalanceActivity.update_deposits_from_logs
  end

  def fetching_stubs_for_third_fetch_helper
    # anything in block 1 is confirmed and 2 is pending
    ENV["NUM_OF_REQUIRED_CONFIRMATIONS"] = 1.to_s
    allow(Client).to receive(:eth_block_number)
                 .and_return(fetching_eth_block_number("0x05"))
    updated_logs = fetching_get_third_raw_logs_helper
    seventh_receipt = fetching_get_seventh_receipt_helper
    eighth_receipt = fetching_get_eighth_receipt_helper
    allow(Contract).to receive(:create_client).and_return(Client)
    allow(Client).to receive(:eth_get_logs).and_return(updated_logs)
    allow(Client).to receive(:eth_get_transaction_receipt)
    .and_return(seventh_receipt, eighth_receipt)
  end

  def fetching_stubs_for_3rd_fetch_helper
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    fetching_stubs_for_second_fetch_helper
    BalanceActivity.update_deposits_from_logs
    fetching_stubs_for_third_fetch_helper
  end

  def fetching_3_should_now_have_8_bas_and_4_accounts_helper
    fetch_logs_3_times_helper
    expect(Account.count).to eq(4)
    expect(BalanceActivity.count).to eq(8)
  end

  def fetching_3_should_update_one_account_and_two_bas_helper
    fetching_stubs_for_3rd_fetch_helper
    expect_any_instance_of(Account).to receive(:update!).once
    save_count = 0
    allow_any_instance_of(BalanceActivity).to receive(:update!) do |arg|
      save_count += 1
    end
    BalanceActivity.update_deposits_from_logs
    expect(save_count).to eq(2)
  end

  def fetching_3_creates_ba_has_old_account_updates_balances_helper
    fetch_logs_3_times_helper
    ba = BalanceActivity.order("id ASC").last
    expect(ba.account.balance_activities.count).to eq(3)
    expect(ba["transaction_status"]).to eq("success")
    expect(ba["confirmation_status"]).to eq("confirmed")
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(1.0e+18)
    expect(ba.old_balance).to eq(5.0e+17)
    expect(ba.account.balance).to eq(1.0e+18)
  end
  def fetching_3_creates_ba_has_old_account_doesnt_update_balances_helper
    fetch_logs_3_times_helper
    ba = BalanceActivity.order("id ASC").second_to_last
    expect(ba.account.balance_activities.count).to eq(3)
    expect(ba["transaction_status"]).to eq("revert")
    expect(ba["confirmation_status"]).to eq("confirmed")
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(nil)
    expect(ba.old_balance).to eq(nil)
    expect(ba.account.balance).to eq(0.0)
  end
  def fetching_3_sets_activity_type_credit_or_debit_and_gas_helper
    fetch_logs_3_times_helper
    BalanceActivity.all.each do |ba|
      expect(ba["activity_type"]).to eq("deposit")
      expect(ba["credit_or_debit"]).to eq("credit")
      expect(ba["gas_used"]).to eq(23145)
    end
  end
  def fetching_3_sets_created_at_block_number_for_accounts_helper
    fetch_logs_3_times_helper
    expect(Account.where(created_at_block_number: 1, balance: 0.0).count)
          .to eq(1)
    expect(Account.where(created_at_block_number: 1, balance: 5.0e+17).count)
          .to eq(1)
    expect(Account.where(created_at_block_number: 2, balance: 0.0).count)
          .to eq(1)
    expect(Account.where(created_at_block_number: 2, balance: 1.0e+18).count)
          .to eq(1)
  end

  ##################################################
  # SECOND FETCH
  ##################################################
  def fetching_stubs_for_second_fetch_helper
    # anything in block 1 is confirmed and 2 is pending
    ENV["NUM_OF_REQUIRED_CONFIRMATIONS"] = 1.to_s
    allow(Client).to receive(:eth_block_number)
                 .and_return(fetching_eth_block_number("0x03"))
    updated_logs = fetching_get_second_raw_logs_helper
    first_receipt = fetching_get_first_receipt_helper
    second_receipt = fetching_get_second_receipt_helper
    third_receipt = fetching_get_third_receipt_helper
    fourth_receipt = fetching_get_fourth_receipt_helper
    fifth_receipt = fetching_get_fifth_receipt_helper
    sixth_receipt = fetching_get_sixth_receipt_helper
    allow(Contract).to receive(:create_client).and_return(Client)
    allow(Client).to receive(:eth_get_logs).and_return(updated_logs)
    allow(Client).to receive(:eth_get_transaction_receipt)
    .and_return(first_receipt, second_receipt, third_receipt, fourth_receipt,
                fifth_receipt, sixth_receipt)
  end

  def fetching_stubs_for_both_fetch_helper
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    fetching_stubs_for_second_fetch_helper
  end

  def fetching_2_doesnt_update_accounts_it_shouldnt_helper
    fetching_stubs_for_both_fetch_helper
    expect_any_instance_of(Account).to receive(:update!).once
    BalanceActivity.update_deposits_from_logs
  end
  def fetching_2_doesnt_update_bas_it_shouldnt_helper
    fetching_stubs_for_both_fetch_helper
    save_count = 0
    allow_any_instance_of(BalanceActivity).to receive(:update!) do |arg|
      save_count += 1
    end
    BalanceActivity.update_deposits_from_logs
    expect(save_count).to eq(2)
  end

  def fetching_2_should_create_new_ba_with_previous_account_and_not_update_balances_helper
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: "success",
      confirmation_status: "pending"
    }
    first_ba = BalanceActivity.find_by(filter)
    account = first_ba.account

    # SECOND
    fetching_stubs_for_second_fetch_helper
    BalanceActivity.update_deposits_from_logs
    account.reload
    new_ba = account.balance_activities.order("id ASC").last
    expect(account.balance_activities.count).to eq(2)
    expect(new_ba["confirmation_status"]).to eq("pending")
    expect(new_ba.account).to eq(account)
    expect(new_ba.amount).to eq(5.0e+17)
    expect(new_ba.new_balance).to eq(nil)
    expect(new_ba.old_balance).to eq(nil)
    expect(new_ba.account.balance).to eq(5.0e+17)
  end
  def fetching_2_should_change_one_ba_to_confirmed_and_not_update_balances_helper
    # FIRST
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: "revert",
      confirmation_status: "pending"
    }
    ba = BalanceActivity.find_by(filter)
    account = ba.account
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(nil)
    expect(ba.old_balance).to eq(nil)
    expect(ba.account.balance).to eq(0.0)

    # SECOND
    fetching_stubs_for_second_fetch_helper
    BalanceActivity.update_deposits_from_logs
    ba.reload
    account.reload
    expect(ba["transaction_status"]).to eq("revert")
    expect(ba["confirmation_status"]).to eq("confirmed")
    expect(ba.account).to eq(account)
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(nil)
    expect(ba.old_balance).to eq(nil)
    expect(ba.account.balance).to eq(0.0)
  end
  def fetching_2_should_change_one_ba_to_confirmed_and_update_balances_helper
    # FIRST
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: "success",
      confirmation_status: "pending"
    }
    ba = BalanceActivity.find_by(filter)
    account = ba.account
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(nil)
    expect(ba.old_balance).to eq(nil)
    expect(ba.account.balance).to eq(0.0)

    # SECOND
    fetching_stubs_for_second_fetch_helper
    BalanceActivity.update_deposits_from_logs
    ba.reload
    account.reload
    expect(ba["transaction_status"]).to eq("success")
    expect(ba["confirmation_status"]).to eq("confirmed")
    expect(ba.account).to eq(account)
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(5.0e+17)
    expect(ba.old_balance).to eq(0.0)
    expect(ba.account.balance).to eq(5.0e+17)
  end

  def fetching_2_should_now_have_6_bas_and_4_accounts_helper
    expect(Account.count).to eq(0)
    expect(BalanceActivity.count).to eq(0)
    fetching_stubs_for_both_fetch_helper
    BalanceActivity.update_deposits_from_logs
    expect(Account.count).to eq(4)
    expect(BalanceActivity.count).to eq(6)
  end
  ##################################################
  # FIRST FETCH
  ##################################################
  def fetching_stubs_for_first_fetch_helper
    # anything in block 1 is confirmed and 2 is pending
    ENV["NUM_OF_REQUIRED_CONFIRMATIONS"] = 1.to_s
    allow(Client).to receive(:eth_block_number)
                 .and_return(fetching_eth_block_number("0x02"))
    first_logs = fetching_get_first_raw_logs_helper
    first_receipt = fetching_get_first_receipt_helper
    second_receipt = fetching_get_second_receipt_helper
    third_receipt = fetching_get_third_receipt_helper
    fourth_receipt = fetching_get_fourth_receipt_helper
    allow(Contract).to receive(:create_client).and_return(Client)
    allow(Client).to receive(:eth_get_logs).and_return(first_logs)
    allow(Client).to receive(:eth_get_transaction_receipt)
    .and_return(first_receipt, second_receipt, third_receipt, fourth_receipt)
  end
  def fetching_should_have_one_account_one_ba_helper(txn_status, conf_status)
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: txn_status,
      confirmation_status: conf_status
    }
    bas = BalanceActivity.where(filter)
    expect(bas.count).to eq(1)
    expect(bas.first.account).not_to eq(nil)
  end
  def fetching_should_have_ba_with_0_balance_helper(txn_status, conf_status)
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: txn_status,
      confirmation_status: conf_status
    }
    ba = BalanceActivity.find_by(filter)
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(nil)
    expect(ba.old_balance).to eq(nil)
    expect(ba.account.balance).to eq(0.0)
  end
  def fetching_should_have_success_confirmed_ba_with_updated_balances_helper
    fetching_stubs_for_first_fetch_helper
    BalanceActivity.update_deposits_from_logs
    filter = {
      transaction_status: "success",
      confirmation_status: "confirmed"
    }
    ba = BalanceActivity.find_by(filter)
    expect(ba.amount).to eq(5.0e+17)
    expect(ba.new_balance).to eq(5.0e+17)
    expect(ba.old_balance).to eq(0.0)
    expect(ba.account.balance).to eq(5.0e+17)
  end
  def fetching_should_have_one_account_one_success_confirmed_ba_helper
    fetching_should_have_one_account_one_ba_helper("success", "confirmed")
  end
  def fetching_should_have_revert_confirmed_ba_with_0_balance_helper
    fetching_should_have_ba_with_0_balance_helper("revert", "confirmed")
  end
  def fetching_should_have_one_account_one_revert_confirmed_ba_helper
    fetching_should_have_one_account_one_ba_helper("revert", "confirmed")
  end
  def fetching_should_have_success_pending_ba_with_0_balance_helper
    fetching_should_have_ba_with_0_balance_helper("success", "pending")
  end
  def fetching_should_have_one_account_one_success_pending_ba_helper
    fetching_should_have_one_account_one_ba_helper("success", "pending")
  end
  def fetching_should_have_revert_pending_ba_with_0_balance_helper
    fetching_should_have_ba_with_0_balance_helper("revert", "pending")
  end
  def fetching_should_have_one_account_one_revert_pending_ba_helper
    fetching_should_have_one_account_one_ba_helper("revert", "pending")
  end
  def fetching_should_have_four_accounts_and_bas_helper
    fetching_stubs_for_first_fetch_helper
    expect(Account.count).to eq(0)
    expect(BalanceActivity.count).to eq(0)
    BalanceActivity.update_deposits_from_logs
    expect(Account.count).to eq(4)
    expect(BalanceActivity.count).to eq(4)
  end

end
