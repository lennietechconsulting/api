module SignatureSpecHelpers

  module SignatureVerifyResponse
    def self.body
      return "true"
    end
  end
  module SignatureVerifyResponseFalse
    def self.body
      return "false"
    end
  end
  ##################################################
  # VERIFY_SIG
  ##################################################
  def sig_verify_sig_returns_true_helper
    url_base = "https://test.com"
    ENV['SIG_API_URL'] = url_base
    data = {}
    sigType = "goalCreate"
    allow(HTTParty).to receive(:post).and_return(SignatureVerifyResponseFalse)
    result = Signature.verify_sig(data, sigType)
    expect(result).to eq(false)
  end
  def sig_verify_sig_returns_false_if_response_body_is_false_helper
    url_base = "https://test.com"
    ENV['SIG_API_URL'] = url_base
    data = {}
    sigType = "goalCreate"
    allow(HTTParty).to receive(:post).and_return(SignatureVerifyResponse)
    result = Signature.verify_sig(data, sigType)
    expect(result).to eq(true)
  end
  def sig_verify_sig_returns_false_if_error_helper
    url_base = "https://test.com"
    ENV['SIG_API_URL'] = url_base
    data = {}
    sigType = "goalCreate"
    allow(HTTParty).to receive(:post).and_throw("error")
    result = Signature.verify_sig(data, sigType)
    expect(result).to eq(false)
  end
  def sig_verify_sig_calls_httparty_post_helper
    url_base = "https://test.com"
    ENV['SIG_API_URL'] = url_base
    data = {}
    sigType = "goalCreate"
    data_with_sig_type = { sigType: sigType }
    expect(HTTParty).to receive(:post)
      .with(url_base + '/verify-sig', body: JSON.parse(data_with_sig_type.to_json))
      .and_return(SignatureVerifyResponse)
    Signature.verify_sig(data, sigType)
  end

end
