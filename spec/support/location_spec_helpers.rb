module LocationSpecHelpers

  ##################################################
  # .CLOSE_ENOUGH
  ##################################################
  def location_close_enough_returns_true_helper
    address_to_compare = {
      latitude: 1,
      longitude: 2
    }
    other_lat_lng = {
      currentLatitude: 3,
      currentLongitude: 4
    }
    allow(Geocoder::Calculations).to receive(:distance_between)
                                  .and_return(0.0521371)
    result = Location.close_enough(address_to_compare, other_lat_lng)
    expect(result).to eq(true)
  end
  def location_close_enough_returns_false_helper
    address_to_compare = {
      latitude: 1,
      longitude: 2
    }
    other_lat_lng = {
      currentLatitude: 3,
      currentLongitude: 4
    }
    allow(Geocoder::Calculations).to receive(:distance_between)
                                  .and_return(0.0721371)
    result = Location.close_enough(address_to_compare, other_lat_lng)
    expect(result).to eq(false)
  end
  def location_close_enough_calls_distance_between_helper
    address_to_compare = {
      latitude: 1,
      longitude: 2
    }
    other_lat_lng = {
      currentLatitude: 3,
      currentLongitude: 4
    }
    expect(Geocoder::Calculations).to receive(:distance_between)
                                  .with([1,2], [3,4])
                                  .and_return(0)
    Location.close_enough(address_to_compare, other_lat_lng)
  end

end
