module B2TestHelper

  module ResponseFailDouble
    def self.body
      return true
    end
    def self.code
      "400"
    end
    def self.headers
      {"x-bz-content-sha1" => "02083f4579e08a612425c0c1a17ee47add783b94"}
    end
  end

  module ResponseCorruptDouble
    def self.body
      return "body"
    end
    def self.code
      200
    end
    def self.headers
      {"x-bz-content-sha1" => "0204579e08a612425c0c1a17ee47add783b94"}
    end
  end

  module ResponseDouble
    def self.body
      return "body"
    end
    def self.code
      200
    end
    def self.headers
      {"x-bz-content-sha1" => "02083f4579e08a612425c0c1a17ee47add783b94"}
    end
  end
  ##################################################
  # #AUTHORIZE
  ##################################################
  module AuthHelpers
    def b2_auth_token_helper
      b2_auth_response_symbolized_helper[:authorizationToken]
    end

    def b2_stub_auth_token_helper
      allow_any_instance_of(B2).to(
        receive(:authToken).and_return(b2_auth_token_helper))
    end

    def b2_download_url_helper
      b2_auth_response_symbolized_helper[:downloadUrl]
    end

    def b2_stub_download_url_helper
      allow_any_instance_of(B2).to(
        receive(:downloadUrl).and_return(b2_download_url_helper))
    end

    def b2_auth_url_helper
      "https://#{B2::ACCOUNT_ID}" #:#{B2::APP_KEY}@#{B2::AUTHORIZE_LINK}"
    end

    def b2_stub_authorize_helper b2, failed=false
      response = failed.present? ? b2_failed_auth_response_helper : b2_auth_response_helper
      auth = ActionController::HttpAuthentication::Basic.encode_credentials(
        B2::ACCOUNT_ID, B2::APP_KEY)
      allow(B2).to receive(:get).with("https://#{B2::AUTHORIZE_LINK}",
                                       {headers: {"Authorization" => auth}})
                                         .and_return(response)
    end

    def b2_auth_response_helper
      {"accountId"=>B2::ACCOUNT_ID,
      "apiUrl"=>"https://api001.backblaze.com",
      "authorizationToken"=>"3_20160206212932_3abb027ae1e6d0f2f4b9ed91_669a95c5db8c9b04c2c31b3bb3cb231a0d09a090_001_acct",
      "downloadUrl"=>"https://f001.backblaze.com"}
    end

    def b2_auth_response_symbolized_helper
      b2_auth_response_helper.deep_symbolize_keys!
    end

    def b2_failed_auth_response_helper
      {"code"=>"bad_auth_token", "message"=>"Invalid authorization token",
       "status"=>401}
    end

    def b2_auth_fail_doesnt_set_instance_variables_helper
      b2 = B2.new
      b2_stub_authorize_helper(b2, "fail")
      b2.authorize
      expect(b2.authToken).to be_nil
      expect(b2.apiUrl).to be_nil
      expect(b2.downloadUrl).to be_nil
    end

    def b2_authorize_sets_instance_variables_correctly_helper
      b2 = B2.new
      b2_stub_authorize_helper(b2)
      b2.authorize
      response = b2_auth_response_helper.deep_symbolize_keys!
      expect(b2.authToken).to eq(response[:authorizationToken])
      expect(b2.apiUrl).to eq(response[:apiUrl])
      expect(b2.downloadUrl).to eq(response[:downloadUrl])
      expect(b2.authToken).to_not be_nil
      expect(b2.apiUrl).to_not be_nil
      expect(b2.downloadUrl).to_not be_nil
    end
    def b2_authorize_sends_out_correct_request_helper
      b2 = B2.new
      auth = ActionController::HttpAuthentication::Basic.encode_credentials(
        B2::ACCOUNT_ID, B2::APP_KEY)
      expect(B2).to receive(:get).with("https://#{B2::AUTHORIZE_LINK}",
                                       {headers: {"Authorization" => auth}})
                .and_return(b2_auth_response_helper)

      b2.authorize
    end
  end

  ##################################################
  # #GET_UPLOAD_URL
  ##################################################
  module GetUploadUrlHelpers
    def b2_get_upload_url_response_helper b2
      {"authorizationToken"=>"3_20160207004822_1d21e108a77a94a7a25efb18_9a426794bba8102232bbf301f7ba174cec41d7d4_001_upld",
       "bucketId"=> B2::POSTGRES_BUCKET_ID,
       "uploadUrl"=>"https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"}.to_json
    end

    def b2_get_upload_url_response_symbolized_helper b2
      JSON.parse(b2_get_upload_url_response_helper(b2)).deep_symbolize_keys!
    end

    def b2_failed_get_upload_url_response_helper b2
      {"code"=>"bad_auth_token", "message"=>"Invalid authorization token", "status"=>401}.to_json
    end

    def b2_stub_get_upload_url_helper b2, failed=false
      b2_stub_authorize_helper(b2)

      if failed.present?
        response = b2_failed_get_upload_url_response_helper b2
      else
        response = b2_get_upload_url_response_helper b2
      end

      path = B2::GET_UPLOAD_URL_PATH

      stub_request(:post, /#{path}/).to_return(:body => response,
        :headers => {"Content-Type"=> "application/json"})
    end
    def b2_get_upload_url_sets_correct_variables_helper
      b2 = B2.new
      b2_stub_get_upload_url_helper(b2)
      b2.get_upload_url B2::POSTGRES_BUCKET_ID
      response = b2_get_upload_url_response_symbolized_helper b2
      expect(b2.uploadAuthToken).to eq(response[:authorizationToken])
      expect(b2.uploadUrl).to eq(response[:uploadUrl])
    end
    def b2_calls_get_upload_url_with_correct_args_helper
      b2 = B2.new
      b2_stub_get_upload_url_helper(b2)
      b2.get_upload_url B2::POSTGRES_BUCKET_ID
      url = b2.apiUrl + B2::GET_UPLOAD_URL_PATH
      expect(a_request(:post, url)
        .with(:body => {"bucketId": B2::POSTGRES_BUCKET_ID}.to_json,
              :headers => {"Authorization" => b2.authToken}))
        .to(have_been_made.once)
    end
    def b2_get_upload_url_does_not_set_instance_variables_helper
      b2 = B2.new
      b2_stub_authorize_helper(b2, "fail")
      b2.authorize
      b2.get_upload_url B2::POSTGRES_BUCKET_ID
      expect(b2.uploadAuthToken).to be_nil
      expect(b2.uploadUrl).to be_nil
    end
    def b2_get_upload_url_does_not_call_get_upload_url_when_unauthorized_helper
      b2 = B2.new
      url = B2::GET_UPLOAD_URL_PATH
      b2_stub_authorize_helper(b2, "fail")
      b2.authorize
      b2.get_upload_url B2::POSTGRES_BUCKET_ID
      expect(a_request(:post, /#{url}/)).not_to have_been_made
    end
  end

  ##################################################
  # #UPLOAD_FILE
  ##################################################
  module UploadFileHelpers
    def b2_upload_file_response_helper
      {"accountId"=>B2::ACCOUNT_ID,
       "bucketId"=>B2::POSTGRES_BUCKET_ID,
       "contentLength"=>7678,
       "contentSha1"=>"174e23326ea1725ca760bef3ec32a096f9fcf7",
       "contentType"=>"application/x-gzip",
       "fileId"=>"4_zedefa07f713b96ae57230f1d_f115ff518ae83411a_d20160207_m022640_c001_v0001005_t0033",
       "fileInfo"=>{},
       "fileName"=>"backup_test"}.to_json
    end

    def b2_upload_file_response_symbolized_helper
      JSON.parse(b2_upload_file_response_helper).deep_symbolize_keys!
    end

    def b2_failed_upload_file_response_helper
      {"error" => "Could not upload file"}.to_json
    end

    def b2_stub_upload_file_helper failed=false
      b2 = B2.new
      b2_stub_get_upload_url_helper b2

      if failed.present?
        response = b2_failed_upload_file_response_helper
      else
        response = b2_upload_file_response_helper
      end

      url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"

      stub_request(:post, url).to_return(:body => response,
        :headers => {"Content-Type"=> "application/json"})
    end
    def b2_upload_file_returns_correct_response_helper
      b2_stub_upload_file_helper
      response = B2.upload_file(
        B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
      expect(response).to(eq(b2_upload_file_response_symbolized_helper))
      expect(response[:contentLength]).to eq(7678)
    end
    def b2_upload_file_makes_correct_request_helper
      b2_stub_upload_file_helper
      B2.upload_file(B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
      url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"

      upload_info = b2_get_upload_url_response_symbolized_helper B2.new

      expect(a_request(:post, url).with(
        :body => File.read("tmp/backup_test.tar.gz"),
        :headers => {
          "Authorization" => upload_info[:authorizationToken],
          "X-Bz-File-Name" => "tmp/backup_test",
          "Content-Type" => "application/x-gzip",
          "X-Bz-Content-Sha1" => 'ed15c0835e2e6cdc0135e94fdadfee1f70e2974e',
          "Content-Length" => "5133"
        })).to have_been_made.once
    end
    def b2_upload_file_upload_fails_returns_error_helper
      b2_stub_upload_file_helper("fail")
      expect(B2.upload_file(
        B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip"
      )[:error]).to(eq("Could not upload file"))
    end
    def b2_upload_file_upload_fails_makes_upload_request_helper
      b2_stub_upload_file_helper("fail")
      B2.upload_file(B2::POSTGRES_BUCKET_ID, "tmp/backup_test.tar.gz", "application/x-gzip")
      url = "https://pod-000-1018-07.backblaze.com/b2api/v1/b2_upload_file/#{B2::POSTGRES_BUCKET_ID}/c001_v0001018_t0015"
      expect(a_request(:post, url)).to have_been_made.once
    end
    def b2_upload_file_returns_error_helper
      b2 = B2.new
      b2_stub_get_upload_url_helper(b2, "fail")
      expect(B2.upload_file(B2::POSTGRES_BUCKET_ID, "fake", "fake")).to(
      eq({"error" => "Could not get upload url"}))
    end
    def b2_upload_file_doesnt_call_upload_when_get_upload_url_fails_helper
      b2 = B2.new
      b2_stub_get_upload_url_helper(b2, "fail")
      B2.upload_file B2::POSTGRES_BUCKET_ID, "fake", "fake"
      expect(a_request(:post, /https/)).to have_been_made.once
    end
  end

  module DownloadFileHelpers

    def b2_prepare_download_file_helper failed=false, corrupt=false
      b2 = B2.new
      b2_stub_authorize_helper b2
      b2_stub_auth_token_helper
      b2_stub_download_url_helper
      b2_stub_download_file_helper failed, corrupt
    end

    def b2_test_download_request_fail_returns_error
      b2_prepare_download_file_helper('fail')
      download_headers = {headers: {"Authorization" => b2_auth_token_helper}}
      allow(B2).to receive(:get).with(b2_download_url_full_helper,download_headers)
                .and_return(ResponseFailDouble)
      expect(call_download_file_by_name).to eq(
        JSON.parse(b2_failed_download_response_helper).deep_symbolize_keys!
      )
    end

    def b2_test_download_success_returns_proper_response
      b2_prepare_download_file_helper
      download_headers = {headers: {"Authorization" => b2_auth_token_helper}}
      allow(B2).to receive(:get).with(b2_download_url_full_helper,download_headers)
                .and_return(ResponseDouble)
      expect(call_download_file_by_name[:body]).to eq(
        ResponseDouble.body
      )
      expect(call_download_file_by_name[:headers]).to eq(
        ResponseDouble.headers
      )
    end

    def b2_test_download_success_with_bad_sha1_returns_error
      b2_prepare_download_file_helper false, 'corrupt'
      download_headers = {headers: {"Authorization" => b2_auth_token_helper}}
      expect(B2).to receive(:get).with(b2_download_url_full_helper,download_headers)
                .and_return(ResponseCorruptDouble)
      expect(call_download_file_by_name).to eq(
        b2_download_corrupt_response_helper
      )
    end

    def b2_test_download_request_is_sent_with_proper_data_and_headers_helper
      b2_prepare_download_file_helper 'fail'
      download_headers = {headers: {"Authorization" => b2_auth_token_helper}}
      expect(B2).to receive(:get).with(b2_download_url_full_helper,download_headers)
                .and_return(ResponseDouble)
      call_download_file_by_name
    end

    def b2_download_url_full_helper
      "#{b2_download_url_helper}/file/postgres-backups/tmp/backup_test.tar.gz"
    end

    def b2_test_download_request_not_sent_if_auth_fails
      allow_any_instance_of(B2).to receive(:authorize).and_return(nil)
      expect(a_request(:get, b2_download_url_full_helper)).not_to have_been_made
    end

    def call_download_file_by_name
      B2.download_file_by_name "postgres-backups", "tmp/backup_test.tar.gz"
    end

    def b2_test_that_download_returns_error_if_auth_fails
      allow_any_instance_of(B2).to receive(:authorize).and_return(nil)
      error = {status: 500, message: "B2 could not authenticate"}
      expect(call_download_file_by_name).to eq(error)
      expect(call_download_file_by_name).not_to eq("hello")
    end

    def b2_test_that_authorize_is_called_during_download
      expect_any_instance_of(B2).to receive(:authorize).once
      call_download_file_by_name
    end

    def b2_failed_download_auth_response_helper
      {status: 500, message: "B2 could not authenticate"}.to_json
    end

    def b2_failed_download_response_helper
      return {status: 500, message: "Download file failed"}.to_json
    end

    def b2_download_corrupt_response_helper
      return {status: 500, message: "file corrupt"}
    end

    def b2_download_response_helper corrupt=false
      sha1 = corrupt.present? ? "corruptsha1" : "174e23326efca1725ca760bef3ec32a096f9fcf7"
      response = {}
      response[:headers] = {"server"=>["Apache-Coyote/1.1"], "x-content-type-options"=>["nosniff"], "x-xss-protection"=>["1; mode=block"], "x-frame-options"=>["SAMEORIGIN"], "cache-control"=>["max-age=0, no-cache, no-store"], "x-bz-file-name"=>["tmp/backup_test"], "x-bz-file-id"=>["4_zedefa07f713b96ae57230f1d_f115ff518ae83411a_d20160207_m022640_c001_v0001005_t0033"], "x-bz-content-sha1"=>[sha1], "accept-ranges"=>["bytes"], "content-type"=>["application/x-gzip"], "content-length"=>["7678"], "date"=>["Mon, 08 Feb 2016 01:10:15 GMT"], "connection"=>["close"]}
      response[:body] = File.read("tmp/backup_test.tar.gz").force_encoding("ISO-8859-1")
      response
    end

    def b2_download_response_symbolized_helper
      JSON.parse(b2_download_response_helper).deep_symbolize_keys!
    end

    def b2_stub_download_file_helper failed=false, corrupt=false
      if failed.present?
        body = b2_failed_download_response_helper
        headers = {"Content-Type"=> "application/json"}
        status = 500
      else
        response = b2_download_response_helper corrupt
        headers = response[:headers]
        body = response[:body]
        status = 200
      end

      stub_request(:get, b2_download_url_full_helper).to_return(
        :body => body,
        :headers => headers,
        :status => status)
    end
  end
end
