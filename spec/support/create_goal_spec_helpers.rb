module CreateGoalSpecHelpers

  def goal_stubs_helper
    allow(Signature).to receive(:verify_sig).and_return(true)
  end
  def goal_create_data_helper(start_time, end_time)
    {
      startTimeHuman: "Wed Jun 13 2018 23:20:47 GMT-0400 (Eastern Daylight Time)",
      startTimeUnix: start_time,
      endTimeHuman: "Thu Jun 14 2018 16:20:00 GMT-0400 (Eastern Daylight Time)",
      endTimeUnix: end_time,
      amount: "0.001",
      formattedAddress: "3325 Kelton Ave, Los Angeles, CA 90034, USA",
      latitude: "34.02427339999999",
      longitude: "-118.4187063",
      signature: "0xb5fc55ae8660b241778caa6d99181e229567dc795c2e44d7bbb"+
                   "aea9db3e699e453814bc474f25f99bc2e50e655954586b4a7c53d"+
                   "46aa2b53dbc1c2ce38d46b4a1c",
      coinbase: "0x83047ecdaf3c2dfb0e9850bdb3311d8d5cb20e39"
    }
  end
  def goal_user_can_create_goal_helper(options={})
    account = create(:account, balance: 1000000000000000000)
    if options[:expired]
      start_time = (Time.now - 20.seconds).to_i.to_s
      end_time = (Time.now - 10.seconds).to_i.to_s
    else
      start_time = Time.now.to_i.to_s
      end_time = (Time.now + 30.seconds).to_i.to_s
    end
    data = goal_create_data_helper(start_time, end_time)
    data[:coinbase] = "0x" + account[:eth_address]
    goal_stubs_helper

    expect(Goal.count).to eq(0)
    expect(Address.count).to eq(0)

    Goal.create_goal(data)

    expect(Goal.count).to eq(1)
    expect(Address.count).to eq(1)

    # test account and account balance
    goal = Goal.first
    expect(account[:balance]).to eq(1.0e+18)
    expect(account.usable_balance).to eq(9.99e+17)
    expect(account.usable_balance).to eq(account[:balance] - goal[:amount])

    # test goal creation
    expect(goal[:signature]).to eq(data[:signature])
    expect(goal[:amount]).to eq(1.0e+15)
    expect(goal[:start_time_unix]).to eq(start_time.to_i)
    expect(goal[:start_time_human]).to eq(data[:startTimeHuman])
    expect(goal[:end_time_unix]).to eq(end_time.to_i)
    expect(goal[:end_time_human]).to eq(data[:endTimeHuman])
    expect(goal[:status]).to eq("pending")
    expect(goal[:goal_type]).to eq("location_arrival")
    expect(goal.account).to eq(account)
    expect(goal.address).not_to eq(nil)

    # test address creation
    address = goal.address
    expect(address[:latitude]).to eq(34.0242734)
    expect(address[:longitude]).to eq(-118.4187063)
    expect(address[:formatted_address]).to eq(data[:formattedAddress])
    expect(address.addressable).to eq(goal)
  end

end
