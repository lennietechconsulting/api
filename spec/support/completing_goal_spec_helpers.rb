module CompletingGoalSpecHelpers

  def goal_completion_data_helper
    {
      goalId: Goal.first[:id],
      currentLatitude: "45.5121481",
      currentLongitude: "-73.5750371",
      signature: "0x145e124172f1498531f11afe98ba3806f6bbca4adaa"+
                 "83894582d7b822e8a862c6c6745e51896bb6f7f4de7a5"+
                 "167c19929cfe1b7f0037a4bc96b216ee4241823f1b",
     coinbase: "0x83047ecdaf3c2dfb0e9850bdb3311d8d5cb20e39"
    }
  end
  def goal_completion_updates_goal_and_user_not_charged_helper
    goal_user_can_create_goal_helper
    data = goal_completion_data_helper
    data[:coinbase] = Account.first[:eth_address]
    allow(Signature).to receive(:verify_sig).and_return(true)
    allow(Location).to receive(:close_enough).and_return(true)
    Goal.attempt_to_update_as_completed(data)
    expect(Goal.count).to eq(1)
    goal = Goal.first
    account = goal.account
    expect(goal[:status]).to eq("success")
    expect(account[:balance]).to eq(1.0e+18)
    expect(account.usable_balance).to eq(1.0e+18)
  end
  def goal_completion_updates_expired_goal_and_user_is_charged_helper
    goal_user_can_create_goal_helper(expired: true)
    data = goal_completion_data_helper
    data[:coinbase] = Account.first[:eth_address]
    allow(Signature).to receive(:verify_sig).and_return(true)
    allow(Location).to receive(:close_enough).and_return(true)
    expect(BalanceActivity.count).to eq(0)
    Goal.attempt_to_update_as_completed(data)
    expect(Goal.count).to eq(1)
    goal = Goal.first
    account = goal.account
    expect(goal[:status]).to eq("failed")
    expect(account[:balance]).to eq(9.99e+17)
    expect(account.usable_balance).to eq(9.99e+17)

    ba = BalanceActivity.first
    expect(BalanceActivity.count).to eq(1)
    expect(goal.balance_activities.first).to eq(goal.balance_activities.first)
    expect(ba[:activity_type]).to eq("charge")
    expect(ba[:amount]).to eq(goal[:amount])
    expect(ba[:credit_or_debit]).to eq("debit")
    expect(ba.itemable).to eq(goal)
    expect(ba[:new_balance]).to eq(9.99e+17)
    expect(ba[:old_balance]).to eq(1.0e+18)
    expect(ba.account).to eq(account)
  end
end
