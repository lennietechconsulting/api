# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_05_04_205753) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "eth_address"
    t.float "balance"
    t.integer "created_at_block_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["eth_address"], name: "index_accounts_on_eth_address", unique: true
  end

  create_table "addresses", force: :cascade do |t|
    t.float "latitude"
    t.float "longitude"
    t.string "formatted_address"
    t.string "addressable_type"
    t.bigint "addressable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["addressable_type", "addressable_id"], name: "index_addresses_on_addressable_type_and_addressable_id"
  end

  create_table "balance_activities", force: :cascade do |t|
    t.binary "transaction_hash"
    t.integer "activity_type"
    t.float "amount"
    t.integer "credit_or_debit"
    t.string "itemable_type"
    t.bigint "itemable_id"
    t.float "new_balance"
    t.float "old_balance"
    t.integer "account_id"
    t.integer "block_number"
    t.integer "gas_used"
    t.integer "transaction_status"
    t.integer "confirmation_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_balance_activities_on_account_id"
    t.index ["activity_type"], name: "index_balance_activities_on_activity_type"
    t.index ["confirmation_status", "transaction_status"], name: "index_balance_act_on_both_statuses"
    t.index ["confirmation_status"], name: "index_balance_activities_on_confirmation_status"
    t.index ["itemable_type", "itemable_id"], name: "index_balance_activities_on_itemable_type_and_itemable_id"
    t.index ["transaction_hash"], name: "index_balance_activities_on_transaction_hash", unique: true
    t.index ["transaction_status"], name: "index_balance_activities_on_transaction_status"
  end

  create_table "goals", force: :cascade do |t|
    t.binary "signature"
    t.float "amount"
    t.integer "start_time_unix"
    t.string "start_time_human"
    t.integer "end_time_unix"
    t.string "end_time_human"
    t.integer "status"
    t.integer "goal_type"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_goals_on_account_id"
    t.index ["goal_type"], name: "index_goals_on_goal_type"
    t.index ["signature"], name: "index_goals_on_signature", unique: true
    t.index ["status"], name: "index_goals_on_status"
  end

end
