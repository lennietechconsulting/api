class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.float :latitude
      t.float :longitude
      t.string :formatted_address
      t.references :addressable, polymorphic: true, index: true

      t.timestamps
    end
  end
end
