class CreateGoals < ActiveRecord::Migration[5.2]
  def change
    create_table :goals do |t|
      t.binary :signature
      t.float :amount
      t.integer :start_time_unix
      t.string :start_time_human
      t.integer :end_time_unix
      t.string :end_time_human
      t.integer :status
      t.integer :goal_type
      t.integer :account_id

      t.timestamps
    end
    add_index :goals, :status
    add_index :goals, :signature, unique: true
    add_index :goals, :goal_type
    add_index :goals, :account_id
  end
end
