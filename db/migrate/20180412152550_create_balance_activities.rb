class CreateBalanceActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :balance_activities do |t|
      t.binary :transaction_hash, unique: true
      t.integer :activity_type
      t.float :amount
      t.integer :credit_or_debit
      t.references :itemable, polymorphic: true, index: true
      t.float :new_balance
      t.float :old_balance
      t.integer :account_id
      t.integer :block_number
      t.integer :gas_used
      t.integer :transaction_status
      t.integer :confirmation_status

      t.timestamps
    end
    add_index :balance_activities, :transaction_hash, unique: true
    add_index :balance_activities, :activity_type
    add_index :balance_activities, :account_id
    add_index :balance_activities, :transaction_status
    add_index :balance_activities, :confirmation_status
    add_index :balance_activities, [:confirmation_status, :transaction_status],
              name: "index_balance_act_on_both_statuses"
  end
end
