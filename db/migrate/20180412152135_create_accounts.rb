class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :eth_address, unique: true
      t.float :balance
      t.integer :created_at_block_number

      t.timestamps
    end
    add_index :accounts, :eth_address, unique: true
  end
end
