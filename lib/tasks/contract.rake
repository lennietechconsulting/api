namespace :contract do
  desc "update balances and accounts from contract"
  task :fetch_new_deposits => :environment do
    BalanceActivity.update_deposits_from_logs
  end
end
