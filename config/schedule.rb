#Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "/var/log/cron_log.log"
env :PATH, ENV['PATH']
ENV.each { |k, v| env(k, v)   }
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end
every 1.day, at: '2:30 am' do
 rake "data:backup_and_push"
end

every 5.minutes do
 rake "contract:fetch_new_deposits"
end

# Learn more: http://github.com/javan/whenever

