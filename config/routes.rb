Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/accounts/:address' => 'accounts#update_account_and_return'
  resources :goals, only: [:create, :index]
  patch '/goals/submit-completion' => 'goals#submit_completion'
end
