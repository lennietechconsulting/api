#! /bin/bash

host=$1
port=$2
user=$3
database=$4
backup_file_name=$5

echo "Dumping database"
pg_dump -h $host -p $port -U $user -Ft -d $database | gzip > $backup_file_name
echo "Database dumped"
