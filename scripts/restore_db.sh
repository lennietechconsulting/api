#! /bin/bash

host=$1
port=$2
user=$3
database=$4
gz_file_name=$5
tar_file_name=$6

echo "Dropping Database"
dropdb -h $host -p $port -U $user $database
echo "Database Dropped"

echo "Creating Database"
createdb -h $host -p $port -U $user $database
echo "Database Created"

echo "Restoring Database"
pg_restore -h $host -p $port -U $user -d $database | gzip > $tar_file_name
echo "Database Restored"
