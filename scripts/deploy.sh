#! /bin/bash

echo "Building docker image"
docker build -t mlennie/snifme-api .

echo "stopping and removing api1"
docker stop api1 && docker rm api1

echo "running api1"
docker run -d --net mynetwork --name api1 -e "environment=production" mlennie/snifme-api

echo "connecting api1 to data_network"
docker network connect data_network api1

echo "stopping and removing api2"
docker stop api2 && docker rm api2

echo "running api2"
docker run -d --net mynetwork --name api2 -e "environment=production" mlennie/snifme-api

echo "connecting api2 to data_network"
docker network connect data_network api2

echo "resetting ssh"
service ssh restart

