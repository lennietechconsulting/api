class AccountsController < ApplicationController

  def update_account_and_return
    json = Account.update_data_and_return_account(params[:address])
    render json: json
  end
end

