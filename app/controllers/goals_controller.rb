class GoalsController < ApplicationController
  def create
    render Goal.create_goal(params)
  end

  def index
    render Goal.get_list_for_eth_address(params[:ethAddress])
  end

  def submit_completion
    status = Goal.attempt_to_update_as_completed(params) ? 204 : 400
    render status: status
  end
end
