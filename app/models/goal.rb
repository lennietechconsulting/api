class Goal < ApplicationRecord
  has_one :address, as: :addressable
  belongs_to :account
  has_many :balance_activities, as: :itemable

  validates_presence_of :signature, :amount, :status, :goal_type,
                        :account_id, :end_time_unix, :end_time_human,
                        :start_time_unix, :start_time_human
  validates_numericality_of :amount, greater_than: 0
  validates_uniqueness_of :signature
  validate :start_time_greater_than_last_30_seconds
  validate :end_time_greater_than_start_time

  enum goal_type:  { location_arrival: 0 }
  enum status:  { pending: 0, success: 1, failed: 2 }

  def start_time_greater_than_last_30_seconds
    #TODO test only on create
    return unless self.new_record?
    begin
      if self.start_time_unix < (Time.now - 30.seconds).to_i
        errors.add(:start_time_unix, "more than 30 seconds old")
      end
    rescue
    end
  end

  def end_time_greater_than_start_time
    #TODO test only on create
    return unless self.new_record?
    begin
      if self.end_time_unix <= self.start_time_unix
        errors.add(:end_time_unix, "must be greater than start time")
      end
    rescue
    end
  end

  def self.get_list_for_eth_address(eth_address)
    account = Account.find_by_address(eth_address)

    return { status: 400 } if !account

    goals = account.goals.includes(:address).map do |goal|
      address = goal.address
      {
        id: goal[:id],
        amount: goal[:amount],
        startTime: goal[:start_time_human],
        endTime: goal[:end_time_human],
        status: goal[:status],
        latitude: address[:latitude],
        longitude: address[:longitude],
        formattedAddress: address[:formatted_address]
      }
    end

    return { json: goals, status: 200 }
  end

  def self.update_all_pending_expired_goals
    self.update_pending_expired_goals(self)
  end

  def self.get_newly_expired_and_pending
    return self.includes(:account)
               .where(status: "pending")
               .where("end_time_unix <= ?", Time.now.to_i)
  end

  def update_status_and_charge_if_should(new_status)
    return unless self[:status] === "pending"

    self.update!(status: new_status)
    if self[:status] === "failed"
      BalanceActivity.create_charge_ba_and_charge_account_for_goal_fail(self)
    end
  end

  def self.update_pending_expired_goals(goals_to_filter)
    ActiveRecord::Base.transaction do
      goals_to_filter.get_newly_expired_and_pending.each do |goal|
        goal.update_status_and_charge_if_should("failed")
      end
    end
  end

  def update_as_success_or_fail_if_should(params)
    ActiveRecord::Base.transaction do
      return unless self[:status] == "pending"
      return unless Location.close_enough(self.address, params)
      new_status = Time.now.to_i >= self[:end_time_unix] ? "failed" : "success"
      self.update_status_and_charge_if_should(new_status)
    end
  end

  def self.verify_goal_completion_and_return_goal(params)
    return false unless Signature.verify_sig(params, "goalFinish")
    goal = Goal.find_by(id: params[:goalId])
    return false unless goal && (goal[:status] === "pending")
    coinbase = Account.format_address(params[:coinbase])
    return false unless goal.account[:eth_address] === coinbase
    return goal
  end

  def self.attempt_to_update_as_completed(params)
    valid_goal = self.verify_goal_completion_and_return_goal(params)
    return false unless valid_goal
    valid_goal.update_as_success_or_fail_if_should(params)
    return valid_goal[:status] != "pending"
  end

  def self.validate_create_goal_data(data)
    return false if Goal.find_by(signature: data[:signature])
    return false unless Signature.verify_sig(data, "goalCreate")
    return false unless data[:account]
    return false if data[:amount_in_wei] > data[:account].usable_balance
    return true
  end

  def self.add_extras_to_data(data)
    data[:amount_in_wei] = EthHelpers.to_wei(data[:amount])
    account = Account.find_by_address(data[:coinbase])
    data[:account] = account
    data
  end

  # TODO test upates
  # TODO notify admin when goal cannot be created
  def self.create_goal(data)

    data = self.add_extras_to_data(data)

    # TODO test new response here
    return { status: 400 } unless self.validate_create_goal_data(data)

    # TODO test begin and catch
    begin
      ActiveRecord::Base.transaction do
        # create goal
        goal_data = {
          signature: data[:signature],
          amount: data[:amount_in_wei],
          start_time_human: data[:startTimeHuman],
          start_time_unix: data[:startTimeUnix].to_i,
          end_time_human: data[:endTimeHuman],
          end_time_unix: data[:endTimeUnix].to_i,
          status: "pending",
          goal_type: "location_arrival",
          account: data[:account]
        }
        goal = Goal.create!(goal_data)

        # create address
        address_data = {
          latitude: data[:latitude],
          longitude: data[:longitude],
          formatted_address: data[:formattedAddress],
          addressable: goal
        }
        address = Address.create!(address_data)
        return { json: { id: address[:id] }, status: 201 }
      end
    rescue
      return { status: 400 }
    end
  end

end
