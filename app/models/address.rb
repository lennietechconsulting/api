class Address < ApplicationRecord
  belongs_to :addressable, polymorphic: true
  validates_presence_of :latitude, :longitude, :formatted_address, :addressable
end
