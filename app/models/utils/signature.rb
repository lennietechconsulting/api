class Signature < ApplicationRecord

  def self.verify_sig(data, sigType)
    data[:sigType] = sigType
    url = ENV['SIG_API_URL'] + '/verify-sig'
    response = {}
    begin
      response = HTTParty.post(url, body: JSON.parse(data.to_json))
    rescue Exception => e
      Rails.logger.error e
      return false
    end
    return response.body === "true"
  end

end
