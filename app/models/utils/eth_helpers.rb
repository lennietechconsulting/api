class EthHelpers < ApplicationRecord

  def self.to_wei(ether)
    ether.to_f * 1000000000000000000
  end

  def self.to_ether(wei)
    wei.to_f / 1000000000000000000
  end


end
