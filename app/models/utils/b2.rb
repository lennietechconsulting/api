require 'digest/sha1'

class B2

  include HTTParty
  debug_output $stdout

  attr_accessor :authToken, :apiUrl, :downloadUrl, :uploadAuthToken, :uploadUrl

  ACCOUNT_ID = ['B2_ACCOUNT_ID']
  APP_KEY = ENV['B2_APP_KEY']
  POSTGRES_BUCKET_ID = ENV['B2_BUCKET_ID']
  AUTHORIZE_LINK ="api.backblaze.com/b2api/v1/b2_authorize_account"
  GET_UPLOAD_URL_PATH = "/b2api/v1/b2_get_upload_url"

  def authorize
    url = AUTHORIZE_LINK
    auth = ActionController::HttpAuthentication::Basic.encode_credentials(
    ACCOUNT_ID, APP_KEY)

    response = B2.get("https://#{url}", {headers: {"Authorization" => auth}}).deep_symbolize_keys!

    if response[:authorizationToken].present? &&
       response[:apiUrl].present? &&
       response[:downloadUrl].present?

      self.authToken = response[:authorizationToken]
      self.apiUrl = response[:apiUrl]
      self.downloadUrl = response[:downloadUrl]
    else
      Rails.logger.error "B2 authentication failed. Response: #{response}"
    end
  end

  def get_upload_url bucket_id
    authorize
    return unless authToken.present?

    url = apiUrl + GET_UPLOAD_URL_PATH
    options = {}
    options[:body] = {"bucketId": bucket_id}.to_json
    options[:headers] = {"Authorization" => authToken}

    response = B2.post(url,options).deep_symbolize_keys!

    if response[:authorizationToken].present? &&
       response[:uploadUrl].present?

      self.uploadAuthToken = response[:authorizationToken]
      self.uploadUrl = response[:uploadUrl]
    else
      Rails.logger.error "B2 get_upload_url failed. Response: #{response}"
    end
  end

  def self.upload_file bucket_id, file, content_type

    b2 = B2.new
    b2.get_upload_url bucket_id
    unless b2.uploadUrl.present?
      return {"error" => "Could not get upload url"}
    end

    options = {}
    options[:headers] = {
      "Authorization" => b2.uploadAuthToken,
      "X-Bz-File-Name" => file.split(".")[0],
      "Content-Type" => content_type,
      "X-Bz-Content-Sha1" => Digest::SHA1.hexdigest(File.read(file)),
      "Content-Length" => File.size(file).to_s
    }
    options[:body] = File.read(file)

    B2.post(b2.uploadUrl,options).deep_symbolize_keys!
  end

  def self.download_file_by_name bucket_name, file_name
    b2 = B2.new
    b2.authorize

    unless b2.authToken.present?
      return {status: 500, message: "B2 could not authenticate"}
    end

    url = "#{b2.downloadUrl}/file/#{bucket_name}/#{file_name}"
    options = {}
    options[:headers] = {"Authorization" => b2.authToken}

    response = B2.get(url,options)
    unless response.body.present? &&
           response.code == 200
      return {status: 500, message: "Download file failed"}
    else
      file = response.body
      headers = response.headers

      sha1 = Digest::SHA1.hexdigest(file)
      unless headers["x-bz-content-sha1"] === sha1
        return {status: 500, message: "file corrupt"}
      else
        return {
          body: response.body,
          headers: response.headers
        }
      end
    end
  end

  def self.get_image_extension(local_file_path)
    png = Regexp.new("\x89PNG".force_encoding("binary"))
    jpg = Regexp.new("\xff\xd8\xff\xe0\x00\x10JFIF".force_encoding("binary"))
    jpg2 = Regexp.new("\xff\xd8\xff\xe1(.*){2}Exif".force_encoding("binary"))
    case IO.read(local_file_path, 10)
    when /^GIF8/
      'gif'
    when /^#{png}/
      'png'
    when /^#{jpg}/
      'jpg'
    when /^#{jpg2}/
      'jpg'
    else
      mime_type = `file #{local_file_path} --mime-type`.gsub("\n", '') # Works on linux and mac
      raise UnprocessableEntity, "unknown file type" if !mime_type
      mime_type.split(':')[1].split('/')[1].gsub('x-', '').gsub(/jpeg/, 'jpg').gsub(/text/, 'txt').gsub(/x-/, '')
    end
  end

end
