class Contract < ApplicationRecord

  CONTRACT_ADDRESS = ENV["ALARM_CONTRACT_ADDRESS"]

  def self.create_client
    Ethereum::HttpClient.new(ENV["CLIENT_ADDRESS"])
  end

  # This is the max block number to consider a transaction confirmed
  def self.get_max_confirmed_block_number(client)
    current_block_number = client.eth_block_number["result"].to_i(16)
    current_block_number - ENV["NUM_OF_REQUIRED_CONFIRMATIONS"].to_i
  end

  def self.get_raw_logs(client, options={})
    to_block = options[:to_block] || 'latest'

    log_params = {
      fromBlock: options[:from_block] || '0x0',
      toBlock: to_block,
      address: CONTRACT_ADDRESS,
      topics: options[:topics] || []
    }

    client.eth_get_logs(log_params)
  end

  def self.get_data_types(data)
    if (data.length / 64) > 2
      return ["address", "address", "uint256", "uint256"]
    else
      return ["address", "uint256"]
    end
  end

  # rinkeby sometimes has 2 logs. ["logs"][0] and ["logs"][1]
  # and address is second data item and amount is 4th item
  # ganache and mainnet has one log where address is first item in log
  # and amount is second item in log,
  def self.get_args_for_transaction(transaction)
    decoder = Ethereum::Decoder.new
    logs = transaction["result"]["logs"]
    log = logs[1] ? logs[1] : logs[0]
    data = log["data"].gsub(/^0x/,'')
    data_types = self.get_data_types(data)
    return data_types.each.with_index.map do |t , i|
      decoder.decode(t, data, i*64)
    end
  end

  def self.get_confirmation_status(block_number, max_confirmed_block_number)
    block_number <= max_confirmed_block_number ? "confirmed" : "pending"
  end

  def self.get_values_from_args(args)
    account_address = args[3] ? args[1] : args[0]
    amount = args[3] ? args[3] : args[1]
    return account_address, amount
  end
  # TODO test
  def self.get_integer_from_hash(value)
    if value.is_a? Integer
      return value
    elsif value.split("")[0] === "0" and value.split("")[1] === "x"
      return value.to_i(16)
    else
      return value.to_i
    end
  end

  # on rinkeby got "0x1" for status
  # in rinkeby address is args[1] and amount is args[3]
  def self.get_formatted_data_from_raw_logs(client, logs)
    max_conf_number = self.get_max_confirmed_block_number(client)
    return logs["result"].map do |log|
      transaction = client.eth_get_transaction_receipt(log["transactionHash"])
      args = self.get_args_for_transaction(transaction)
      result = transaction["result"]
      block_number = result["blockNumber"].to_i(16)
      conf_status = self.get_confirmation_status(block_number, max_conf_number)
      account_address, amount = self.get_values_from_args(args)
      #TODO test calling get_formatted_gas_used and status
      gas_used = self.get_integer_from_hash(result["gasUsed"])
      txn_status = self.get_integer_from_hash(result["status"])
      {
        account_address: account_address,
        amount: amount,
        transaction_hash: result["transactionHash"],
        block_number: block_number,
        gas_used: gas_used,
        transaction_status: txn_status,
        confirmation_status: conf_status
      }
    end
  end

  def self.get_logs(options={})
    client = self.create_client
    raw_logs = self.get_raw_logs(client, options)
    self.get_formatted_data_from_raw_logs(client, raw_logs)
  end

end
