class Account < ApplicationRecord

  has_many :balance_activities
  has_many :goals

  validates_presence_of :eth_address, :balance, :created_at_block_number
  validates_uniqueness_of :eth_address
  validates_numericality_of :created_at_block_number, greater_than: 0
  validates_length_of :eth_address, is: 40

  def valid_deposits
    self.balance_activities.valid_deposits
  end

  def self.update_data_and_return_account(address)
    account = self.find_by_address(address)
    return "fail" unless account
    Goal.update_pending_expired_goals(account.goals)
    return {
      address: account[:eth_address],
      balance: account[:balance],
      usable_balance: account.usable_balance
    }
  end

  def self.create_account(account_info)
    self.create!({
      eth_address: account_info[:account_address],
      balance: 0.00,
      created_at_block_number: account_info[:block_number]
    })
  end

  def self.find_or_create_account(account_info)
    account_info[:account_address] = self.format_address(
                                           account_info[:account_address])
    self.find_by(eth_address: account_info[:account_address]) ||
    self.create_account(account_info)
  end

  def self.find_by_address(address)
    self.find_by(eth_address: self.format_address(address))
  end

  def self.format_address(address)
    address.gsub(/0x/,'')
  end

  def sum_of_total_pending_charges
    self.goals.where(status: "pending").pluck(:amount).sum
  end

  def usable_balance
    pending_charges = self.sum_of_total_pending_charges
    self[:balance] - pending_charges
  end

end
